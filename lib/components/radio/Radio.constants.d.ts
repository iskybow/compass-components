import type { TRadioSizeToken, TRadioDefinition } from './Radio.types';
declare const DEFAULT_RADIO_SIZE: TRadioSizeToken;
declare const RADIO_SIZES: TRadioSizeToken[];
declare const RADIO_VALUES_MAPPING: Record<TRadioSizeToken, TRadioDefinition>;
export { RADIO_SIZES, DEFAULT_RADIO_SIZE, RADIO_VALUES_MAPPING };
