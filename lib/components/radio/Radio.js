var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import { DEFAULT_RADIO_SIZE } from './Radio.constants';
import RadioRoot from './Radio.root';
import RadioLabelRoot from './RadioLabel.root';
const Radio = (props) => {
    const {
            label,
            size = DEFAULT_RADIO_SIZE,
            hasError = false,
            disabled = false,
            checked = false,
            onChange = Utils.noop,
        } = props,
        rest = __rest(props, ['label', 'size', 'hasError', 'disabled', 'checked', 'onChange']);
    const hasLabel = Utils.isString(label) && label.length > 0;
    const rootProperties = Object.assign({ size, hasError, disabled, checked }, rest);
    return React.createElement(
        RadioRoot,
        Object.assign({}, rootProperties),
        React.createElement('input', { type: 'checkbox', checked: checked, onChange: onChange }),
        React.createElement('div', null),
        hasLabel && React.createElement(RadioLabelRoot, { size: size }, label)
    );
};
export default Radio;
