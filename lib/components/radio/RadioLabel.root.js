import styled, { css } from 'styled-components';
import Spacing, { applyMargin } from '../../utilities/spacing';
import { applyTextMargin, applyTextStyles } from '../text';
import { Utils } from '../../shared';
import { RADIO_VALUES_MAPPING } from './Radio.constants';
const RadioLabelRoot = styled.span.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property) && validator(property),
})(({ size }) => {
    // @default: `size === 'md'`
    let labelMargin = 125;
    if (size === 'sm') {
        labelMargin = 100;
    } else if (size === 'lg') {
        labelMargin = 150;
    }
    return css`
        ${applyTextStyles({
            inheritLineHeight: true,
            size: RADIO_VALUES_MAPPING[size].labelSize,
        })};
        ${applyTextMargin({ margin: 'none' })};
        ${applyMargin(Spacing.only('left', labelMargin))};
    `;
});
export default RadioLabelRoot;
