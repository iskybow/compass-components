const DEFAULT_RADIO_SIZE = 'md';
const RADIO_SIZES = ['sm', 'md', 'lg'];
const RADIO_VALUES_MAPPING = {
    sm: {
        radioSize: 12,
        labelSize: 75,
    },
    md: {
        radioSize: 16,
        labelSize: 100,
    },
    lg: {
        radioSize: 20,
        labelSize: 200,
    },
};
export { RADIO_SIZES, DEFAULT_RADIO_SIZE, RADIO_VALUES_MAPPING };
