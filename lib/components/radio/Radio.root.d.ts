declare const RadioRoot: import("styled-components").StyledComponent<"label", any, Required<Pick<import("./Radio.props").default, "size" | "children" | "disabled" | "checked" | "hasError">>, never>;
export default RadioRoot;
