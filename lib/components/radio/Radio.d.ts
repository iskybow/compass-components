import React from 'react';
import type PRadio from './Radio.props';
declare const Radio: React.FC<PRadio>;
export default Radio;
