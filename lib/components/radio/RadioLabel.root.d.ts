declare const RadioLabelRoot: import("styled-components").StyledComponent<"span", any, Required<Pick<import("./Radio.props").default, "size">>, never>;
export default RadioLabelRoot;
