import type { TIconSize } from '../../foundations/icon';
import type { TTextSizeToken } from '../text';
declare type TRadioSizeToken = 'sm' | 'md' | 'lg';
declare type TRadioSize = Extract<TIconSize, 12 | 16 | 20>;
declare type TRadioDefinition = {
    radioSize: TRadioSize;
    labelSize: TTextSizeToken;
};
export type { TRadioSizeToken, TRadioSize, TRadioDefinition };
