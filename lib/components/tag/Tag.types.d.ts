import type { TTextSizeToken } from '../text';
declare type TLabelTagVariant = 'general' | 'info' | 'warning' | 'success';
declare type TTagVariant = 'highlight' | 'shortcut' | TLabelTagVariant;
export type { TTextSizeToken as TTagSizeToken, TTagVariant };
