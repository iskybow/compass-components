var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import TagRoot from './Tag.root';
import { DEFAULT_TAG_VARIANT, DEFAULT_TAG_SIZE } from './Tag.constants';
const Tag = (props) => {
    const { variant = DEFAULT_TAG_VARIANT, size = DEFAULT_TAG_SIZE, text } = props,
        rest = __rest(props, ['variant', 'size', 'text']);
    const hasText = Utils.isString(text) && text.length > 0;
    Utils.assert(
        hasText,
        'Tag: This component is intended to be used with text. Please provide a string to it for correct usage.'
    );
    return React.createElement(
        TagRoot,
        Object.assign({ element: 'span', size: size, variant: variant }, rest),
        text
    );
};
export default Tag;
