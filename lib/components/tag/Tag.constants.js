const TAG_VARIANTS = ['highlight', 'shortcut', 'general', 'info', 'warning', 'success'];
const TAG_SIZES = [25, 50, 75, 100, 200, 300];
const DEFAULT_TAG_VARIANT = 'general';
const DEFAULT_TAG_SIZE = 100;
export { TAG_VARIANTS, DEFAULT_TAG_VARIANT, TAG_SIZES, DEFAULT_TAG_SIZE };
