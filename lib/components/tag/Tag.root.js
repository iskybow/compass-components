import styled, { css } from 'styled-components';
import Text, { applyTextMargin, applyTextStyles } from '../text';
import { applyShape } from '../../foundations/shape';
import { Utils } from '../../shared';
import Spacing, { applyPadding } from '../../utilities/spacing';
const TagRoot = styled(Text).withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property) && validator(property),
})((props) => {
    const { variant, size, onClick, theme } = props;
    const TAG_BACKGROUND_COLOR_MAP = {
        general: theme.background.skeleton,
        info: theme.palette.primary.light,
        warning: theme.palette.alert.light,
        success: theme.palette.success.light,
        highlight: theme.highlight.mention,
        shortcut: theme.background.skeleton,
    };
    return css`
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: ${TAG_BACKGROUND_COLOR_MAP[variant]};
        color: ${variant === 'highlight' ? theme.palette.primary.main : theme.text.primary};
        text-transform: ${variant === 'highlight' ? 'none' : 'uppercase'};
        cursor: ${Utils.isFunction(onClick) ? 'pointer' : 'inherit'};

        ${applyPadding(
            Spacing.symmetric({
                vertical: 0,
                horizontal: 50,
            })
        )};

        ${applyShape({
            radius: 4,
            width: 'auto',
            height: 'auto',
        })};

        ${applyTextMargin({ margin: 'none' })};
        ${applyTextStyles({ size, weight: variant === 'highlight' ? 'regular' : 'bold' })};
    `;
});
export default TagRoot;
