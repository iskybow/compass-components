/// <reference types="react" />
import type { PTagRoot } from './Tag.props';
declare const TagRoot: import("styled-components").StyledComponent<import("react").FC<import("../text/Text.props").default>, any, PTagRoot, never>;
export default TagRoot;
