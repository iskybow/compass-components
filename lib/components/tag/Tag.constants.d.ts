import type { TTagVariant, TTagSizeToken } from './Tag.types';
declare const TAG_VARIANTS: TTagVariant[];
declare const TAG_SIZES: TTagSizeToken[];
declare const DEFAULT_TAG_VARIANT: TTagVariant;
declare const DEFAULT_TAG_SIZE: TTagSizeToken;
export { TAG_VARIANTS, DEFAULT_TAG_VARIANT, TAG_SIZES, DEFAULT_TAG_SIZE };
