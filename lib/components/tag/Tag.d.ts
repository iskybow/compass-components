/// <reference types="react" />
import type PTag from './Tag.props';
declare const Tag: (props: PTag) => JSX.Element;
export default Tag;
