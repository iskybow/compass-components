var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import {
    DEFAULT_TEXT_COLOR,
    DEFAULT_TEXT_ELEMENT,
    DEFAULT_TEXT_MARGIN,
    DEFAULT_TEXT_SIZE,
    DEFAULT_TEXT_WEIGHT,
    TEXT_COLORS,
    TEXT_ELEMENTS,
    TEXT_MARGINS,
    TEXT_SIZES,
    TEXT_WEIGHTS,
} from './Text.constants';
import TextRoot from './Text.root';
const Text = (props) => {
    const {
            inheritLineHeight = false,
            color = DEFAULT_TEXT_COLOR,
            element = DEFAULT_TEXT_ELEMENT,
            margin = DEFAULT_TEXT_MARGIN,
            size = DEFAULT_TEXT_SIZE,
            weight = DEFAULT_TEXT_WEIGHT,
        } = props,
        rest = __rest(props, ['inheritLineHeight', 'color', 'element', 'margin', 'size', 'weight']);
    Utils.assert(
        TEXT_ELEMENTS.includes(element) || React.isValidElement(element),
        `Text: component was used with an unsupported element '${element}'.
            Please provide one from these available options: ${TEXT_ELEMENTS.join(
                ', '
            )}, or a valid \`ReactElement\``,
        true
    );
    Utils.assert(
        TEXT_COLORS.includes(color) || Utils.isColor(color),
        `Text: component was used with an unsupported color '${color}'.
            Please provide one from these available options: ${TEXT_WEIGHTS.join(
                ', '
            )}, or a valid CSS color value`,
        true
    );
    Utils.assert(
        TEXT_SIZES.includes(size),
        `Text: component was used with an unsupported size '${size}'.
            Please provide one from these available options: ${TEXT_SIZES.join(', ')}.`,
        true
    );
    Utils.assert(
        TEXT_MARGINS.includes(margin),
        `Text: component was used with an unsupported margin '${margin}'.
            Please provide one from these available options: ${TEXT_MARGINS.join(', ')}.`,
        true
    );
    Utils.assert(
        TEXT_WEIGHTS.includes(weight),
        `Text: component was used with an unsupported weight '${weight}'.
            Please provide one from these available options: ${TEXT_WEIGHTS.join(', ')}.`,
        true
    );
    const rootProperties = Object.assign({ inheritLineHeight, color, margin, size, weight }, rest);
    return React.createElement(TextRoot, Object.assign({ as: element }, rootProperties));
};
export default Text;
