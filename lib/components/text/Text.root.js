import styled, { css } from 'styled-components';
import { Utils } from '../../shared';
import { applyTextColor, applyTextMargin, applyTextStyles } from './Text.mixins';
const TextRoot = styled.p.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property, ['color', 'size', 'margin', 'weight']) && validator(property),
})((props) => {
    const { theme, inheritLineHeight, color, margin, size, weight } = props;
    return css`
        ${applyTextStyles({ inheritLineHeight, size, weight })};
        ${applyTextColor({ color, theme })};
        ${applyTextMargin({ margin, size })};

        body.enable-animations & {
            transition: color ${theme.animation.fastest} 0s ease-in-out;
        }
    `;
});
export default TextRoot;
