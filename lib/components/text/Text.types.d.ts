import type { TFontColor, TFontMargin, TFontWeight } from '../../shared';
declare type TTextSizeToken = 25 | 50 | 75 | 100 | 200 | 300;
declare type TTextDefinition = {
    size: number;
    lineHeight: number;
    margin?: number;
};
declare type TTextDefinitionMap = {
    [key in TTextSizeToken]: TTextDefinition;
};
declare type TTextElement = 'p' | 'span' | 'label';
export type { TTextSizeToken, TTextDefinition, TTextDefinitionMap, TTextElement, TFontWeight as TTextWeight, TFontColor as TTextColor, TFontMargin as TTextMargin, };
