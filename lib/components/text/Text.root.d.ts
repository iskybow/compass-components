declare const TextRoot: import("styled-components").StyledComponent<"p", any, Required<Omit<import("./Text.props").default, "className" | "element">>, never>;
export default TextRoot;
