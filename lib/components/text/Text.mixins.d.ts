import type { FlattenSimpleInterpolation } from 'styled-components';
import type { PApplyTextColor, PApplyTextMargin, PApplyTextStyles } from './Text.props';
declare const applyTextStyles: ({ inheritLineHeight, size, weight, }: PApplyTextStyles) => FlattenSimpleInterpolation;
declare const applyTextMargin: ({ margin, size, }: PApplyTextMargin) => FlattenSimpleInterpolation;
declare const applyTextColor: ({ color, theme }: PApplyTextColor) => FlattenSimpleInterpolation;
export { applyTextStyles, applyTextColor, applyTextMargin };
