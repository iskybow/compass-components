import type { TComponentSizeToken } from '../../shared';
export declare type TMentionBadgeSizeToken = Exclude<TComponentSizeToken, 'xxxs' | 'xxs' | 'xs' | 'xl' | 'xxl' | 'xxxl'>;
