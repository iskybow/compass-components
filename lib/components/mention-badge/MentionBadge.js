var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import {
    DEFAULT_MENTIONBADGE_SIZE,
    DEFAULT_MENTIONBAGDE_MENTION_LIMIT,
    DEFAULT_MENTIONBAGDE_MENTIONS,
} from './MentionBadge.constants';
import MentionBadgeRoot from './MentionBadge.root';
const MentionBadge = (props) => {
    const {
            mentions = DEFAULT_MENTIONBAGDE_MENTIONS,
            mentionLimit = DEFAULT_MENTIONBAGDE_MENTION_LIMIT,
            size = DEFAULT_MENTIONBADGE_SIZE,
            inverted = false,
            className,
        } = props,
        rest = __rest(props, ['mentions', 'mentionLimit', 'size', 'inverted', 'className']);
    const isUnreadBadge = mentions === 0;
    const rootProperties = {
        className,
        size,
        inverted,
        isUnreadBadge,
        mentionStringLength: mentions.toString().length,
    };
    return React.createElement(
        MentionBadgeRoot,
        Object.assign({}, rootProperties, rest),
        !isUnreadBadge && (mentions > mentionLimit ? `${mentionLimit}+` : mentions)
    );
};
export default MentionBadge;
