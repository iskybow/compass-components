import type { PMentionBadgeRoot } from './MentionBadge.props';
declare const MentionBadgeRoot: import("styled-components").StyledComponent<"div", any, PMentionBadgeRoot, never>;
export default MentionBadgeRoot;
