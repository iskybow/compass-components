import React from 'react';
import type PMentionBadge from './MentionBadge.props';
declare const MentionBadge: React.FC<PMentionBadge>;
export default MentionBadge;
