var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import {
    BUTTON_SIZE_MAP,
    BUTTON_SIZES,
    BUTTON_VARIANTS,
    DEFAULT_BUTTON_ICON_POSITION,
    DEFAULT_BUTTON_SIZE,
    DEFAULT_BUTTON_VARIANT,
    DEFAULT_BUTTON_WIDTH,
} from './Button.constants';
import ButtonRoot, { ButtonIconRoot } from './Button.root';
const Button = (props) => {
    const {
            label,
            onClick,
            icon,
            iconPosition = DEFAULT_BUTTON_ICON_POSITION,
            size = DEFAULT_BUTTON_SIZE,
            variant = DEFAULT_BUTTON_VARIANT,
            width = DEFAULT_BUTTON_WIDTH,
            active = false,
            destructive = false,
            inverted = false,
            disabled = false,
        } = props,
        rest = __rest(props, [
            'label',
            'onClick',
            'icon',
            'iconPosition',
            'size',
            'variant',
            'width',
            'active',
            'destructive',
            'inverted',
            'disabled',
        ]);
    Utils.assert(
        BUTTON_VARIANTS.includes(variant),
        `Button: The Button component was used with an invalid 'variant' property. Please choose from the following options: ${BUTTON_VARIANTS.join(
            ', '
        )}`,
        true
    );
    Utils.assert(
        BUTTON_SIZES.includes(size),
        `Button: The Button component was used with an invalid 'size' property. Please choose from the following options: ${BUTTON_SIZES.join(
            ', '
        )}`,
        true
    );
    const rootProperties = Object.assign(
        {
            disabled: disabled || !Utils.isFunction(onClick),
            width,
            active,
            destructive,
            inverted,
            size,
            variant,
            onClick,
        },
        rest
    );
    return React.createElement(
        ButtonRoot,
        Object.assign({}, rootProperties),
        icon &&
            iconPosition === 'start' &&
            React.createElement(ButtonIconRoot, {
                glyph: icon,
                size: BUTTON_SIZE_MAP[size].iconSize,
                margin: BUTTON_SIZE_MAP[size].iconMargin,
                marginPosition: 'right',
            }),
        label,
        icon &&
            iconPosition === 'end' &&
            React.createElement(ButtonIconRoot, {
                glyph: icon,
                size: BUTTON_SIZE_MAP[size].iconSize,
                margin: BUTTON_SIZE_MAP[size].iconMargin,
                marginPosition: 'left',
            })
    );
};
export default Button;
