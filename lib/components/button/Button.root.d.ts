/// <reference types="react" />
import type { PButtonIconRoot } from './Button.props';
declare const ButtonIconRoot: import("styled-components").StyledComponent<(props: import("../../foundations/icon/Icon.props").default) => JSX.Element, any, PButtonIconRoot, never>;
declare const ButtonRoot: import("styled-components").StyledComponent<"button", any, Required<Pick<import("./Button.props").default, "size" | "active" | "disabled" | "width" | "inverted" | "variant" | "destructive">>, never>;
export { ButtonIconRoot };
export default ButtonRoot;
