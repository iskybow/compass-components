import type { TIconSize } from '../../foundations/icon';
import type { TComponentSizeToken } from '../../shared';
import type { TSpacingTokensSymmetric } from '../../utilities/spacing';
import type { TTextSizeToken } from '../text';
declare type TButtonSizeToken = Exclude<TComponentSizeToken, 'xxxs' | 'xxs' | 'xl' | 'xxl' | 'xxxl'>;
declare type TButtonWidth = number | 'full' | 'auto';
declare type TButtonVariant = 'primary' | 'secondary' | 'tertiary';
declare type TButtonIconPosition = 'start' | 'end';
declare type TButtonDefinition = {
    spacing: TSpacingTokensSymmetric;
    labelSize: TTextSizeToken;
    iconSize: TIconSize;
    iconMargin: number;
    height: number;
};
export type { TButtonVariant, TButtonSizeToken, TButtonWidth, TButtonIconPosition, TButtonDefinition, };
