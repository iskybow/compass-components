import React from 'react';
import type PButton from './Button.props';
declare const Button: React.FC<PButton>;
export default Button;
