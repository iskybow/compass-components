import type { THeadingSizeToken } from '../heading';
import type { TStatusBadgeSizeToken } from '../status-badge';
import type { TShapeBorderRadius } from '../../foundations/shape';
import type { TAvatarElement, TAvatarSizes, TAvatarSizeToken, TAvatarVariant } from './Avatar.types';
declare const AVATAR_SIZES: TAvatarSizes;
declare const AVATAR_SIZE_LABELS: Record<TAvatarSizeToken, string>;
declare const DEFAULT_AVATAR_SIZE: TAvatarSizeToken;
declare const AVATAR_VARIANTS: TAvatarVariant[];
declare const DEFAULT_AVATAR_VARIANT: TAvatarVariant;
declare const AVATAR_ELEMENTS: TAvatarElement[];
declare const DEFAULT_AVATAR_ELEMENT: TAvatarElement;
declare type TAvatarStatusDefinition = {
    size: TStatusBadgeSizeToken;
    offset: number;
};
declare type TAvatarSizeMap = {
    [key in TAvatarSizeToken]: {
        size: number;
        text: THeadingSizeToken;
        radius: TShapeBorderRadius;
        status: TAvatarStatusDefinition;
    };
};
declare const AVATAR_SIZE_MAP: TAvatarSizeMap;
declare const AVATAR_FALLBACK_COLORS: string[];
export { AVATAR_SIZES, AVATAR_SIZE_LABELS, DEFAULT_AVATAR_SIZE, AVATAR_VARIANTS, DEFAULT_AVATAR_VARIANT, AVATAR_ELEMENTS, DEFAULT_AVATAR_ELEMENT, AVATAR_FALLBACK_COLORS, AVATAR_SIZE_MAP, };
