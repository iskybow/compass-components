/// <reference types="react" />
import type { PAvatarMentionBadgeRoot, PAvatarRoot } from './Avatar.props';
declare const AvatarStatusBadgeRoot: import("styled-components").StyledComponent<import("react").FC<import("../status-badge/StatusBadge.props").default>, any, Required<Pick<PAvatarRoot, "size">> & {
    offset: number;
}, never>;
declare const AvatarMentionBadgeRoot: import("styled-components").StyledComponent<import("react").FC<import("../mention-badge/MentionBadge.props").default>, any, PAvatarMentionBadgeRoot, never>;
declare const AvatarRoot: import("styled-components").StyledComponent<"button", any, PAvatarRoot, never>;
export { AvatarStatusBadgeRoot, AvatarMentionBadgeRoot };
export default AvatarRoot;
