import type { TComponentSizeToken, TComponentSizes } from '../../shared';
import type { TStatusBadgeStatus } from '../status-badge';
declare type TAvatarVariant = 'circle' | 'rounded';
declare type TAvatarElement = 'button' | 'div' | 'span';
export type { TAvatarVariant, TAvatarElement, TComponentSizes as TAvatarSizes, TComponentSizeToken as TAvatarSizeToken, TStatusBadgeStatus as TAvatarStatus, };
