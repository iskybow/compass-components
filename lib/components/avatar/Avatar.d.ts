/// <reference types="react" />
import type PAvatar from './Avatar.props';
declare const Avatar: (props: PAvatar) => JSX.Element;
export default Avatar;
