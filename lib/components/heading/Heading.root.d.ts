declare const HeadingRoot: import("styled-components").StyledComponent<"h6", any, Required<Omit<import("./Heading.props").default, "className" | "element">>, never>;
export default HeadingRoot;
