/// <reference types="react" />
import type PHeading from './Heading.props';
declare const Heading: (props: PHeading) => JSX.Element;
export default Heading;
