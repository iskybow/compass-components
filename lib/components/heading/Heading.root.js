import styled, { css } from 'styled-components';
import { Utils } from '../../shared';
import { applyHeadingColor, applyHeadingMargin, applyHeadingStyles } from './Heading.mixins';
const HeadingRoot = styled.h6.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property) && validator(property),
})((props) => {
    const { theme, inheritLineHeight, color, margin, size, weight } = props;
    return css`
        ${applyHeadingStyles({ inheritLineHeight, size, weight })};
        ${applyHeadingColor({ color, theme })};
        ${applyHeadingMargin({ margin, size })};

        // animation
        body.enable-animations & {
            transition: color ${theme.animation.fastest} 0s ease-in-out;
        }
    `;
});
export default HeadingRoot;
