import type { FlattenSimpleInterpolation } from 'styled-components';
import type { PApplyHeadingColor, PApplyHeadingMargin, PApplyHeadingStyles } from './Heading.props';
declare const applyHeadingStyles: ({ inheritLineHeight, size, weight, }: PApplyHeadingStyles) => FlattenSimpleInterpolation;
declare const applyHeadingMargin: ({ margin, size, }: PApplyHeadingMargin) => FlattenSimpleInterpolation | null;
declare const applyHeadingColor: ({ color, theme }: PApplyHeadingColor) => FlattenSimpleInterpolation;
export { applyHeadingStyles, applyHeadingColor, applyHeadingMargin };
