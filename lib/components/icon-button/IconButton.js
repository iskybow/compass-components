var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import Icon from '../../foundations/icon';
import { Utils } from '../../shared';
import {
    DEFAULT_ICON_BUTTON_ELEMENT,
    DEFAULT_ICON_BUTTON_SIZE,
    ICON_BUTTON_DEFINITIONS,
} from './IconButton.constants';
import IconButtonRoot from './IconButton.root';
/**
 * we need to pass it into `React.forwardRef`, since functional components do
 * not allow refs to be passed on in any other way.
 *
 * This is needed for some components to hold a reference to the `parent`- or
 * `trigger`-component (such as in the `Popover` component)
 */
const IconButton = React.forwardRef((props, reference) => {
    const {
            icon,
            element = DEFAULT_ICON_BUTTON_ELEMENT,
            size = DEFAULT_ICON_BUTTON_SIZE,
            compact = false,
            inverted = false,
            toggled = false,
            active = false,
            destructive = false,
            disabled = false,
            label,
            onClick,
        } = props,
        rest = __rest(props, [
            'icon',
            'element',
            'size',
            'compact',
            'inverted',
            'toggled',
            'active',
            'destructive',
            'disabled',
            'label',
            'onClick',
        ]);
    Utils.assert(
        (!destructive && !toggled) || (destructive && !toggled) || (toggled && !destructive),
        'IconButton: component was used with both `destructive` and `toggled` properties set to true. Please use only one of the options',
        true
    );
    const rootProperties = {
        size,
        compact,
        inverted,
        toggled,
        active,
        destructive,
        disabled,
        onClick,
    };
    return React.createElement(
        IconButtonRoot,
        Object.assign({ ref: reference, as: element }, rootProperties, rest),
        React.createElement(Icon, { glyph: icon, size: ICON_BUTTON_DEFINITIONS[size].iconSize }),
        label && React.createElement('span', null, label)
    );
});
export default IconButton;
