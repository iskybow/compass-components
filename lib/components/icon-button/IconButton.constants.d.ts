import type { TIconButtonSizeToken, TIconButtonDefinition, TIconButtonElement } from './IconButton.types';
declare const ICON_BUTTON_SIZES: TIconButtonSizeToken[];
declare const ICON_BUTTON_SIZE_LABELS: {
    [size in TIconButtonSizeToken]: string;
};
declare const DEFAULT_ICON_BUTTON_SIZE: TIconButtonSizeToken;
declare const ICON_BUTTON_ELEMENTS: TIconButtonElement[];
declare const DEFAULT_ICON_BUTTON_ELEMENT: TIconButtonElement;
/**
 * TODO@all: since the IconButton will be the only component that supports a
 *           compact version we will have an extra property in the definitions
 *           for that. Once we have more than this we should definitely try to
 *           extract that to the `applyMargin` and `applyPadding` functions.
 */
declare const ICON_BUTTON_DEFINITIONS: {
    [size in TIconButtonSizeToken]: TIconButtonDefinition;
};
export { ICON_BUTTON_SIZES, ICON_BUTTON_ELEMENTS, DEFAULT_ICON_BUTTON_ELEMENT, DEFAULT_ICON_BUTTON_SIZE, ICON_BUTTON_SIZE_LABELS, ICON_BUTTON_DEFINITIONS, };
