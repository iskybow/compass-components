declare const IconButtonRoot: import("styled-components").StyledComponent<"button", any, Required<Omit<import("./IconButton.props").default, "className" | "onClick" | "label" | "element" | "icon">>, never>;
export default IconButtonRoot;
