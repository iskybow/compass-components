import React from 'react';
import type PIconButton from './IconButton.props';
/**
 * we need to pass it into `React.forwardRef`, since functional components do
 * not allow refs to be passed on in any other way.
 *
 * This is needed for some components to hold a reference to the `parent`- or
 * `trigger`-component (such as in the `Popover` component)
 */
declare const IconButton: React.ForwardRefExoticComponent<PIconButton & React.RefAttributes<HTMLButtonElement>>;
export default IconButton;
