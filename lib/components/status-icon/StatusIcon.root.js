import styled, { css } from 'styled-components';
import Icon from '../../foundations/icon';
import { Utils } from '../../shared';
const StatusIconRoot = styled(Icon).withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.forceForwardProperty(property, ['glyph', 'size']) ||
        (Utils.blockProperty(property) && validator(property)),
})((props) => {
    const { theme, status } = props;
    return css`
        color: ${theme.badges[status]};
    `;
});
export default StatusIconRoot;
