import type { TStatusIconSizeToken, TStatusIconSizeMap, TStatusIconStatus } from './StatusIcon.types';
declare const STATUSICON_SIZES: TStatusIconSizeToken[];
declare const DEFAULT_STATUSICON_SIZE: TStatusIconSizeToken;
declare const STATUSICON_STATUSES: TStatusIconStatus[];
declare const DEFAULT_STATUSICON_STATUS: TStatusIconStatus;
declare const STATUSICON_SIZE_MAP: TStatusIconSizeMap;
declare const STATUSICON_SIZE_LABELS: Record<TStatusIconSizeToken, string>;
declare const STATUSICON_STATUS_LABELS: Record<TStatusIconStatus, string>;
export { STATUSICON_SIZES, DEFAULT_STATUSICON_SIZE, STATUSICON_SIZE_MAP, STATUSICON_SIZE_LABELS, STATUSICON_STATUSES, DEFAULT_STATUSICON_STATUS, STATUSICON_STATUS_LABELS, };
