const STATUSICON_SIZES = ['xs', 'sm', 'md', 'lg', 'xl'];
const DEFAULT_STATUSICON_SIZE = 'md';
const STATUSICON_STATUSES = ['online', 'away', 'dnd', 'offline'];
const DEFAULT_STATUSICON_STATUS = 'offline';
const STATUSICON_SIZE_MAP = {
    xs: 10,
    sm: 12,
    md: 16,
    lg: 20,
    xl: 32,
};
const STATUSICON_SIZE_LABELS = {
    xs: 'x-small',
    sm: 'small',
    md: 'medium',
    lg: 'large',
    xl: 'x-large',
};
const STATUSICON_STATUS_LABELS = {
    online: 'online',
    away: 'away',
    dnd: 'do not disturb',
    offline: 'offline',
};
export {
    STATUSICON_SIZES,
    DEFAULT_STATUSICON_SIZE,
    STATUSICON_SIZE_MAP,
    STATUSICON_SIZE_LABELS,
    STATUSICON_STATUSES,
    DEFAULT_STATUSICON_STATUS,
    STATUSICON_STATUS_LABELS,
};
