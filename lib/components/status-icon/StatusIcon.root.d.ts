/// <reference types="react" />
declare const StatusIconRoot: import("styled-components").StyledComponent<(props: import("../../foundations/icon/Icon.props").default) => JSX.Element, any, Pick<import("./StatusIcon.props").default, "status"> & {
    size: import("../../foundations/icon/Icon.types").TIconSize;
}, never>;
export default StatusIconRoot;
