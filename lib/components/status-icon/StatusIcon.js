var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { DEFAULT_STATUSICON_SIZE, STATUSICON_SIZE_MAP } from './StatusIcon.constants';
import StatusIconRoot from './StatusIcon.root';
const StatusIcon = (props) => {
    const { status, size = DEFAULT_STATUSICON_SIZE } = props,
        rest = __rest(props, ['status', 'size']);
    let glyph = 'circle-outline';
    switch (status) {
        case 'away':
            glyph = 'clock';
            break;
        case 'dnd':
            glyph = 'minus-circle';
            break;
        case 'online':
            glyph = 'check-circle';
            break;
        case 'offline':
        default:
    }
    const rootProperties = Object.assign({ status, glyph, size: STATUSICON_SIZE_MAP[size] }, rest);
    return React.createElement(StatusIconRoot, Object.assign({}, rootProperties));
};
export default StatusIcon;
