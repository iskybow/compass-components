import React from 'react';
import type PStatusIcon from './StatusIcon.props';
declare const StatusIcon: React.FC<PStatusIcon>;
export default StatusIcon;
