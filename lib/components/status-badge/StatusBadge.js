var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import StatusIcon from '../status-icon';
import { DEFAULT_STATUSBADGE_SIZE } from './StatusBadge.constants';
import StatusBadgeRoot from './StatusBadge.root';
const StatusBadge = (props) => {
    const { status, size = DEFAULT_STATUSBADGE_SIZE } = props,
        rest = __rest(props, ['status', 'size']);
    const rootProperties = Object.assign({ size, status }, rest);
    return React.createElement(
        StatusBadgeRoot,
        Object.assign({}, rootProperties),
        React.createElement(StatusIcon, { status: status, size: size })
    );
};
export default StatusBadge;
