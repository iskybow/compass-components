const STATUSBADGE_SIZES = ['xs', 'sm', 'md', 'lg', 'xl'];
const DEFAULT_STATUSBADGE_SIZE = 'md';
const STATUSBADGE_STATUSES = ['online', 'away', 'dnd', 'offline'];
const DEFAULT_STATUSBADGE_STATUS = 'offline';
const STATUSBADGE_STATUS_LABELS = {
    online: 'online',
    away: 'away',
    dnd: 'do not disturb',
    offline: 'offline',
};
export {
    STATUSBADGE_SIZES,
    DEFAULT_STATUSBADGE_SIZE,
    STATUSBADGE_STATUSES,
    DEFAULT_STATUSBADGE_STATUS,
    STATUSBADGE_STATUS_LABELS,
};
