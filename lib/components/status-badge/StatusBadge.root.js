import styled, { css } from 'styled-components';
import { STATUSICON_SIZE_MAP } from '../status-icon';
import { applyShape } from '../../foundations/shape';
import { Utils } from '../../shared';
const StatusBadgeRoot = styled.div.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property) && validator(property),
})((props) => {
    const { theme, size, background = theme.background.default } = props;
    return css`
        flex: 1;
        display: flex;
        align-items: center;
        justify-content: center;

        background-color: ${background};

        ${applyShape({ width: STATUSICON_SIZE_MAP[size] + 4, radius: 'circle' })};
    `;
});
export default StatusBadgeRoot;
