import type { TStatusBadgeSizeToken, TStatusBadgeStatus } from './StatusBadge.types';
declare const STATUSBADGE_SIZES: TStatusBadgeSizeToken[];
declare const DEFAULT_STATUSBADGE_SIZE: TStatusBadgeSizeToken;
declare const STATUSBADGE_STATUSES: TStatusBadgeStatus[];
declare const DEFAULT_STATUSBADGE_STATUS: TStatusBadgeStatus;
declare const STATUSBADGE_STATUS_LABELS: Record<TStatusBadgeStatus, string>;
export { STATUSBADGE_SIZES, DEFAULT_STATUSBADGE_SIZE, STATUSBADGE_STATUSES, DEFAULT_STATUSBADGE_STATUS, STATUSBADGE_STATUS_LABELS, };
