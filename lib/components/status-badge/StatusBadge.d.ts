import React from 'react';
import type PStatusBadge from './StatusBadge.props';
declare const StatusBadge: React.FC<PStatusBadge>;
export default StatusBadge;
