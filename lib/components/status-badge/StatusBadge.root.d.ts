declare const StatusBadgeRoot: import("styled-components").StyledComponent<"div", any, Required<Pick<import("./StatusBadge.props").default, "size">> & Pick<import("./StatusBadge.props").default, "background">, never>;
export default StatusBadgeRoot;
