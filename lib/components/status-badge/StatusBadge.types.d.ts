import type { TComponentSizeToken, TUserStatus } from '../../shared';
declare type TStatusBadgeSizeToken = Exclude<TComponentSizeToken, 'xxxs' | 'xxs' | 'xxl' | 'xxxl'>;
export type { TUserStatus as TStatusBadgeStatus, TStatusBadgeSizeToken };
