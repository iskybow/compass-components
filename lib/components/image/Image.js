var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React, { useEffect, useState } from 'react';
import { Utils } from '../../shared';
import {
    DEFAULT_IMAGE_BORDER_RADIUS,
    DEFAULT_IMAGE_HEIGHT,
    DEFAULT_IMAGE_WIDTH,
} from './Image.constants';
import ImageRoot from './Image.root';
const Image = (props) => {
    const {
            source,
            alt,
            className,
            fullWidth = false,
            thumbnail = false,
            radius = DEFAULT_IMAGE_BORDER_RADIUS,
            width = DEFAULT_IMAGE_WIDTH,
            height = DEFAULT_IMAGE_HEIGHT,
        } = props,
        rest = __rest(props, [
            'source',
            'alt',
            'className',
            'fullWidth',
            'thumbnail',
            'radius',
            'width',
            'height',
        ]);
    Utils.assert(Boolean(source), 'Image: You need to provide a valid image source');
    const [loading, setLoading] = useState(true);
    const [image, setImage] = useState('');
    useEffect(() => {
        Utils.getBase64(source)
            .then((imageString) => {
                setLoading(false);
                return setImage(imageString);
            })
            .catch(() => {});
    }, [source]);
    const rootProperties = {
        className,
        alt,
        radius,
        thumbnail,
        src: image,
        width: fullWidth ? '100%' : width,
        height: fullWidth ? 'auto' : height,
    };
    return loading
        ? React.createElement('div', { className: 'skeleton' })
        : React.createElement(ImageRoot, Object.assign({}, rootProperties, rest));
};
export default Image;
