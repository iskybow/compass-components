import React from 'react';
import type PImage from './Image.props';
declare const Image: React.FC<PImage>;
export default Image;
