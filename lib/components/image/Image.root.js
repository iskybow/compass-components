import styled, { css } from 'styled-components';
import { Utils } from '../../shared';
import Spacing, { applyPadding } from '../../utilities/spacing';
import { applyShape } from '../../foundations/shape';
const ImageRoot = styled.img.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property) && validator(property),
})((props) => {
    const {
        theme: { background, border },
        width,
        height,
        radius,
        thumbnail,
    } = props;
    const thumbnailStyles = thumbnail
        ? css`
              background-color: ${background.default};
              border: 1px solid ${border.disabled};

              ${applyPadding(Spacing.all(50))};
              ${applyShape({ radius })};
          `
        : null;
    return css`
        display: block;
        margin: auto;

        ${thumbnailStyles}

        ${applyShape({
            width,
            height,
            radius,
        })};
    `;
});
export default ImageRoot;
