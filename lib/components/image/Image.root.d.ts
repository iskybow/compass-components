declare const ImageRoot: import("styled-components").StyledComponent<"img", any, Required<Pick<import("./Image.props").default, "height" | "width" | "radius" | "thumbnail">>, never>;
export default ImageRoot;
