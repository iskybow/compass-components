import type { TShapeBorderRadius } from '../../foundations/shape';
declare const DEFAULT_IMAGE_BORDER_RADIUS: TShapeBorderRadius;
declare const DEFAULT_IMAGE_WIDTH = "auto";
declare const DEFAULT_IMAGE_HEIGHT = "auto";
export { DEFAULT_IMAGE_BORDER_RADIUS, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT };
