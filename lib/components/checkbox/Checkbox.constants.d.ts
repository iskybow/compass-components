import type { TCheckboxSizeToken, TCheckboxDefinition } from './Checkbox.types';
declare const DEFAULT_CHECKBOX_SIZE: TCheckboxSizeToken;
declare const CHECKBOX_SIZES: TCheckboxSizeToken[];
declare const CHECKBOX_VALUES_MAPPING: Record<TCheckboxSizeToken, TCheckboxDefinition>;
export { CHECKBOX_SIZES, DEFAULT_CHECKBOX_SIZE, CHECKBOX_VALUES_MAPPING };
