import React from 'react';
import type PCheckbox from './Checkbox.props';
declare const Checkbox: React.FC<PCheckbox>;
export default Checkbox;
