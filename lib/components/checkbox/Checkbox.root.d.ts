declare const CheckboxRoot: import("styled-components").StyledComponent<"label", any, Required<Pick<import("./Checkbox.props").default, "size" | "disabled" | "checked" | "hasError">>, never>;
export default CheckboxRoot;
