const DEFAULT_CHECKBOX_SIZE = 'md';
const CHECKBOX_SIZES = ['sm', 'md', 'lg'];
const CHECKBOX_VALUES_MAPPING = {
    sm: {
        checkboxSize: 12,
        iconSize: 10,
        labelSize: 75,
    },
    md: {
        checkboxSize: 16,
        iconSize: 12,
        labelSize: 100,
    },
    lg: {
        checkboxSize: 20,
        iconSize: 16,
        labelSize: 200,
    },
};
export { CHECKBOX_SIZES, DEFAULT_CHECKBOX_SIZE, CHECKBOX_VALUES_MAPPING };
