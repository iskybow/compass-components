var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import Icon from '../../foundations/icon';
import Text from '../text';
import { Utils } from '../../shared';
import { CHECKBOX_VALUES_MAPPING, DEFAULT_CHECKBOX_SIZE } from './Checkbox.constants';
import CheckboxRoot from './Checkbox.root';
const Checkbox = (props) => {
    const {
            label,
            size = DEFAULT_CHECKBOX_SIZE,
            checked = false,
            disabled = false,
            hasError = false,
            onChange = Utils.noop,
        } = props,
        rest = __rest(props, ['label', 'size', 'checked', 'disabled', 'hasError', 'onChange']);
    const hasLabel = Utils.isString(label) && label.length > 0;
    const rootProperties = {
        size,
        hasError,
        disabled,
        checked,
    };
    return React.createElement(
        CheckboxRoot,
        Object.assign({}, rootProperties, rest),
        React.createElement('input', { type: 'checkbox', checked: checked, onChange: onChange }),
        React.createElement(
            'div',
            null,
            React.createElement(Icon, {
                glyph: 'check',
                size: CHECKBOX_VALUES_MAPPING[size].iconSize,
            })
        ),
        hasLabel && React.createElement(Text, { element: 'span' }, label)
    );
};
export default Checkbox;
