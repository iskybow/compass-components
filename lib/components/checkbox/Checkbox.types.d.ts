import type { TIconSize } from '../../foundations/icon';
import type { TTextSizeToken } from '../text';
declare type TCheckboxSizeToken = 'sm' | 'md' | 'lg';
declare type TCheckboxSize = Extract<TIconSize, 12 | 16 | 20>;
declare type TCheckboxDefinition = {
    checkboxSize: TCheckboxSize;
    iconSize: TIconSize;
    labelSize: TTextSizeToken;
};
export type { TCheckboxSizeToken, TCheckboxDefinition };
