import type { PTextInputRoot } from './TextInput.props';
declare const TextInputRoot: import("styled-components").StyledComponent<"div", any, PTextInputRoot, never>;
export default TextInputRoot;
