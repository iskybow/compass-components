var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import Icon from '../../foundations/icon';
import Text from '../text';
import {
    DEFAULT_TEXT_INPUT_SIZE,
    TEXT_INPUT_VALUES_MAPPING,
    DEFAULT_TEXT_INPUT_WIDTH,
} from './TextInput.constants';
import TextInputRoot from './TextInput.root';
import InputRoot from './Input.root';
const TextInput = (props) => {
    const {
            label,
            placeholder,
            value,
            leadingIcon,
            trailingIcon,
            backgroundColor,
            size = DEFAULT_TEXT_INPUT_SIZE,
            width = DEFAULT_TEXT_INPUT_WIDTH,
            active = false,
            hasError = false,
            disabled = false,
            animatedLabel = true,
            onClear = Utils.noop,
            onFocus = Utils.noop,
            onChange = Utils.noop,
        } = props,
        rest = __rest(props, [
            'label',
            'placeholder',
            'value',
            'leadingIcon',
            'trailingIcon',
            'backgroundColor',
            'size',
            'width',
            'active',
            'hasError',
            'disabled',
            'animatedLabel',
            'onClear',
            'onFocus',
            'onChange',
        ]);
    const { iconSize } = TEXT_INPUT_VALUES_MAPPING[size];
    const hasLabel = Utils.isString(label) && label.length > 0;
    const rootProperties = {
        size,
        active,
        hasError,
        disabled,
        animatedLabel,
        backgroundColor,
        width,
        leadingIcon,
        onClear,
        value,
        onFocus,
    };
    return React.createElement(
        TextInputRoot,
        Object.assign({}, rootProperties, rest),
        leadingIcon &&
            leadingIcon !== 'none' &&
            React.createElement(Icon, { glyph: leadingIcon, size: iconSize }),
        React.createElement(InputRoot, {
            value: value || '',
            placeholder: Utils.isString(placeholder) && placeholder.length > 0 ? placeholder : '',
            onChange: onChange,
        }),
        hasLabel && React.createElement(Text, { element: 'span' }, label),
        trailingIcon &&
            trailingIcon !== 'none' &&
            React.createElement(Icon, {
                glyph: trailingIcon,
                className: 'trailing-icon',
                size: iconSize,
                onClick: onClear,
            })
    );
};
export default TextInput;
