import React from 'react';
import type PTextInput from './TextInput.props';
declare const TextInput: React.FC<PTextInput>;
export default TextInput;
