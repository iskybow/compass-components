import type { TSpacingToken } from '../../utilities/spacing';
import type { TTextSizeToken } from '../text';
import type { TIconSize } from '../../foundations/icon';
declare type TTextInputSizeToken = 'sm' | 'md' | 'lg';
declare type TTextInputHeight = 32 | 40 | 48;
declare type TTextInput = {
    spacing: TSpacingToken;
    iconSize: TIconSize;
    height: TTextInputHeight;
    labelSize: TTextSizeToken;
    labelMargin: TSpacingToken;
};
export type { TTextInputSizeToken, TTextInput };
