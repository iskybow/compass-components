const DEFAULT_TEXT_INPUT_SIZE = 'md';
const TEXT_INPUT_SIZES = ['sm', 'md', 'lg'];
const DEFAULT_LEADING_ICON = 'magnify';
const DEFAULT_TRAILING_ICON = 'close-circle';
const DEFAULT_TEXT_INPUT_WIDTH = 'full';
const TEXT_INPUT_VALUES_MAPPING = {
    sm: {
        spacing: 125,
        iconSize: 12,
        height: 32,
        labelSize: 75,
        labelMargin: 150,
    },
    md: {
        spacing: 150,
        iconSize: 16,
        height: 40,
        labelSize: 100,
        labelMargin: 175,
    },
    lg: {
        spacing: 150,
        iconSize: 16,
        height: 48,
        labelSize: 200,
        labelMargin: 200,
    },
};
const LABEL_POSITIONS = {
    sm: '-22px, -16px',
    md: '-26px, -20px',
    lg: '-32px, -22px',
};
export {
    DEFAULT_TEXT_INPUT_SIZE,
    TEXT_INPUT_SIZES,
    TEXT_INPUT_VALUES_MAPPING,
    LABEL_POSITIONS,
    DEFAULT_LEADING_ICON,
    DEFAULT_TRAILING_ICON,
    DEFAULT_TEXT_INPUT_WIDTH,
};
