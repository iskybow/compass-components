import type { TIconGlyph } from '../../foundations/icon';
import type { TButtonWidth } from '../button';
import type { TTextInputSizeToken, TTextInput } from './TextInput.types';
declare const DEFAULT_TEXT_INPUT_SIZE: TTextInputSizeToken;
declare const TEXT_INPUT_SIZES: TTextInputSizeToken[];
declare const DEFAULT_LEADING_ICON: TIconGlyph;
declare const DEFAULT_TRAILING_ICON: TIconGlyph;
declare const DEFAULT_TEXT_INPUT_WIDTH: TButtonWidth;
declare const TEXT_INPUT_VALUES_MAPPING: {
    [size in TTextInputSizeToken]: TTextInput;
};
declare const LABEL_POSITIONS: Record<TTextInputSizeToken, string>;
export { DEFAULT_TEXT_INPUT_SIZE, TEXT_INPUT_SIZES, TEXT_INPUT_VALUES_MAPPING, LABEL_POSITIONS, DEFAULT_LEADING_ICON, DEFAULT_TRAILING_ICON, DEFAULT_TEXT_INPUT_WIDTH, };
