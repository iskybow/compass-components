declare const InputRoot: import("styled-components").StyledComponent<"input", any, Required<Pick<import("./TextInput.props").default, "placeholder" | "onChange" | "value">>, never>;
export default InputRoot;
