import type { TTextSizeToken } from '../text';
declare type TSwitchSizeToken = 'sm' | 'md' | 'lg';
declare type TSwitchHeight = 16 | 24 | 32;
declare type TSwitchWidth = 26 | 40 | 52;
declare type TSwitchInnerWidth = 12 | 20 | 26;
declare type TSwitchDefinition = {
    width: TSwitchWidth;
    height: TSwitchHeight;
    innerWidth: TSwitchInnerWidth;
    labelSize: TTextSizeToken;
};
export type { TSwitchSizeToken, TSwitchDefinition, TSwitchWidth, TSwitchHeight, TSwitchInnerWidth };
