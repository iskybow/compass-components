const DEFAULT_SWITCH_SIZE = 'md';
const SWITCH_SIZES = ['sm', 'md', 'lg'];
const SWITCH_VALUES_MAPPING = {
    sm: {
        width: 26,
        height: 16,
        innerWidth: 12,
        labelSize: 75,
    },
    md: {
        width: 40,
        height: 24,
        innerWidth: 20,
        labelSize: 100,
    },
    lg: {
        width: 52,
        height: 32,
        innerWidth: 26,
        labelSize: 200,
    },
};
export { DEFAULT_SWITCH_SIZE, SWITCH_SIZES, SWITCH_VALUES_MAPPING };
