declare const SwitchRoot: import("styled-components").StyledComponent<"label", any, Required<Pick<import("./Switch.props").default, "size" | "onClick" | "disabled" | "toggled">>, never>;
export default SwitchRoot;
