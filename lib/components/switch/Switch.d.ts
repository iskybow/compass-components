import React from 'react';
import type PSwitch from './Switch.props';
declare const Switch: React.FC<PSwitch>;
export default Switch;
