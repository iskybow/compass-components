import type { TSwitchSizeToken, TSwitchDefinition } from './Switch.types';
declare const DEFAULT_SWITCH_SIZE: TSwitchSizeToken;
declare const SWITCH_SIZES: TSwitchSizeToken[];
declare const SWITCH_VALUES_MAPPING: Record<TSwitchSizeToken, TSwitchDefinition>;
export { DEFAULT_SWITCH_SIZE, SWITCH_SIZES, SWITCH_VALUES_MAPPING };
