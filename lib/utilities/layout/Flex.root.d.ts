import type { PFlexRoot } from './Flex.props';
declare const FlexRoot: import("styled-components").StyledComponent<"div", any, PFlexRoot, never>;
export default FlexRoot;
