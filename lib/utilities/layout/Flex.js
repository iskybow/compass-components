var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import {
    DEFAULT_FLEX_ALIGNMENT,
    DEFAULT_FLEX_COMPONENT,
    DEFAULT_FLEX_FLEX,
    DEFAULT_FLEX_JUSTIFY,
    DEFAULT_FLEX_ROW,
    DEFAULT_FLEX_WRAP,
    FLEX_ALIGNMENTS,
    FLEX_ELEMENTS,
    FLEX_JUSTIFIES,
} from './Flex.constants';
import FlexRoot from './Flex.root';
const Flex = function (_a) {
    var {
            element = DEFAULT_FLEX_COMPONENT,
            alignment = DEFAULT_FLEX_ALIGNMENT,
            justify = DEFAULT_FLEX_JUSTIFY,
            flex = DEFAULT_FLEX_FLEX,
            row = DEFAULT_FLEX_ROW,
            wrap = DEFAULT_FLEX_WRAP,
        } = _a,
        rest = __rest(_a, ['element', 'alignment', 'justify', 'flex', 'row', 'wrap']);
    Utils.assert(
        FLEX_ALIGNMENTS.includes(alignment),
        `Compass Components - Flex: incompatible alignment property (${alignment}) set on Flex component. Please choose from the following: ${FLEX_ALIGNMENTS.join(
            ', '
        )}`
    );
    Utils.assert(
        FLEX_JUSTIFIES.includes(justify),
        `Compass Components - Flex: incompatible justify property (${justify}) set on Flex component. Please choose from the following: ${FLEX_JUSTIFIES.join(
            ', '
        )}`
    );
    Utils.assert(
        FLEX_ELEMENTS.includes(element) || Utils.isFunctionalComponent(element),
        `Compass Components - Flex: incompatible element property (${element}) used in Flex component. Please choose from the following: ${FLEX_ELEMENTS.join(
            ', '
        )}`
    );
    const rootProperties = Object.assign({ alignment, justify, flex, row, wrap }, rest);
    return React.createElement(FlexRoot, Object.assign({}, rootProperties, { as: element }));
};
export default Flex;
