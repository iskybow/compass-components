const FLEX_ELEMENTS = [
    'div',
    'span',
    'article',
    'aside',
    'details',
    'figcaption',
    'figure',
    'footer',
    'header',
    'main',
    'mark',
    'nav',
    'section',
    'summary',
    'time',
    'ul',
    'li',
];
const DEFAULT_FLEX_COMPONENT = 'div';
const DEFAULT_FLEX_ROW = false;
const DEFAULT_FLEX_WRAP = false;
const DEFAULT_FLEX_FLEX = 'initial';
const FLEX_ALIGNMENTS = ['initial', 'baseline', 'flex-start', 'center', 'flex-end', 'stretch'];
const DEFAULT_FLEX_ALIGNMENT = 'initial';
const FLEX_JUSTIFIES = [
    'initial',
    'flex-start',
    'center',
    'flex-end',
    'stretch',
    'space-around',
    'space-between',
    'space-evenly',
];
const DEFAULT_FLEX_JUSTIFY = 'initial';
export {
    FLEX_ELEMENTS,
    DEFAULT_FLEX_COMPONENT,
    DEFAULT_FLEX_ROW,
    DEFAULT_FLEX_WRAP,
    DEFAULT_FLEX_FLEX,
    FLEX_ALIGNMENTS,
    DEFAULT_FLEX_ALIGNMENT,
    FLEX_JUSTIFIES,
    DEFAULT_FLEX_JUSTIFY,
};
