/// <reference types="react" />
import type PFlex from './Flex.props';
declare const Flex: <T>({ element, alignment, justify, flex, row, wrap, ...rest }: import("../../shared/props").PGlobalsMisc & import("../../shared/props").PGlobalsLayout & {
    flex?: import("./Flex.types").TFlexFlex | undefined;
    row?: boolean | undefined;
    wrap?: boolean | undefined;
    element?: import("../../shared/types").TContainerElement | undefined;
    alignment?: import("./Flex.types").TFlexAlignment | undefined;
    justify?: import("./Flex.types").TFlexJustify | undefined;
    width?: number | undefined;
    height?: number | undefined;
} & T) => JSX.Element;
export default Flex;
