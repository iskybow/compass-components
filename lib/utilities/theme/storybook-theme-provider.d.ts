/// <reference types="react" />
import type { PThemeProvider } from './theme-provider';
declare const CanvasThemeProvider: ({ children, theme, }: PThemeProvider) => JSX.Element;
declare const DocumentationThemeProvider: ({ children, theme, }: PThemeProvider) => JSX.Element;
export { CanvasThemeProvider, DocumentationThemeProvider };
