import React from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import type { TTheme } from './themes';
declare type PThemeProvider = {
    theme?: TTheme;
    children?: React.ReactNode | React.ReactNode[];
};
declare const ThemeProvider: ({ children, theme }: PThemeProvider) => JSX.Element;
export type { PThemeProvider };
export { StyledThemeProvider as SectionThemeProvider };
export default ThemeProvider;
