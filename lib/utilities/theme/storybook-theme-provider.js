// storybook canvas- & docs-pages style overrides
import React, { useEffect, useState } from 'react';
import { createGlobalStyle } from 'styled-components';
import { setAlpha } from '../../shared';
import ThemeProvider from './theme-provider';
import { lightTheme } from './themes';
const CanvasGlobalStyles = createGlobalStyle`
    body.sb-show-main.sb-main-centered {
        background-color: ${(props) => props.theme.background.default};
        align-items: stretch;

        #root {
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    }
`;
// storybook canvas- & docs-pages style overrides
const DocumentationGlobalStyles = createGlobalStyle`
    body.sb-show-main {
        background-color: ${({ theme }) => theme.background.default};
        align-items: stretch;

        .sbdocs-wrapper {
            background-color: ${({ theme }) =>
                theme.type === 'dark' ? 'transparent' : theme.background.default};

            td {
                background-color: ${({ theme }) =>
                    theme.type === 'dark' ? theme.background.shape : '#FFF'};
            }
            
            h1, h2, h3, h4, h5, h6, p, th, td {
                color: ${({ theme }) => theme.text.primary};
            }
            
            h2 {
                opacity: 0.75;
            
                &:not(.sbdocs-subtitle) {
                    border-bottom: 1px solid ${({ theme }) =>
                        setAlpha(theme.background.contrast, 0.25)};
                }
            }
            
            hr {
                border-top: 1px solid ${({ theme }) => setAlpha(theme.background.contrast, 0.25)};
            }
        }
    }
`;
const CanvasThemeProvider = ({ children = null, theme = lightTheme }) => {
    const [selectedTheme, setSelectedTheme] = useState(theme);
    useEffect(() => {
        setSelectedTheme(theme);
    }, [theme]);
    return React.createElement(
        ThemeProvider,
        { theme: selectedTheme },
        React.createElement(CanvasGlobalStyles, null),
        children
    );
};
const DocumentationThemeProvider = ({ children = null, theme = lightTheme }) => {
    const [selectedTheme, setSelectedTheme] = useState(theme);
    useEffect(() => {
        setSelectedTheme(Object.assign(Object.assign({}, theme), { noStyleReset: true }));
    }, [theme]);
    return React.createElement(
        ThemeProvider,
        { theme: selectedTheme },
        React.createElement(DocumentationGlobalStyles, null),
        children
    );
};
export { CanvasThemeProvider, DocumentationThemeProvider };
