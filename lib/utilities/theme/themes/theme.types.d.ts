import type { TStatusBadgeStatus } from '../../../components/status-badge';
declare type TThemeColorDefinition = {
    lighter: string;
    light: string;
    main: string;
    dark: string;
    darker: string;
    contrast: string;
};
declare type TTHemeColors = {
    primary: TThemeColorDefinition;
    secondary: TThemeColorDefinition;
    alert: TThemeColorDefinition;
    warning: TThemeColorDefinition;
    success: TThemeColorDefinition;
    info: TThemeColorDefinition;
};
declare type TTHemeActionColors = {
    hover: string;
    disabled: string;
};
declare type TTHemeTextColors = {
    primary: string;
    secondary: string;
    disabled: string;
    contrast: string;
    accent: string;
};
declare type TThemeBorderColors = {
    primary: string;
    secondary: string;
    disabled: string;
    contrast: string;
    accent: string;
};
declare type TTHemeBackgroundColors = {
    default: string;
    badge: string;
    shape: string;
    skeleton: string;
    shimmer: string;
    contrast: string;
};
declare type TThemeHighlightColors = {
    mention: string;
};
declare type TThemeBadges = {
    [key in TStatusBadgeStatus]: string;
};
declare type TThemeAnimationSpeed = 'instant' | 'fastest' | 'fast' | 'normal' | 'slow' | 'slowest';
declare type TThemeAnimations = {
    [key in TThemeAnimationSpeed]: number;
};
declare type TTheme = {
    type: 'light' | 'dark';
    noStyleReset: boolean;
    noFontFaces: boolean;
    noDefaultStyle: boolean;
    elevationOpacity: number;
    palette: TTHemeColors;
    badges: TThemeBadges;
    action: TTHemeActionColors;
    text: TTHemeTextColors;
    border: TThemeBorderColors;
    background: TTHemeBackgroundColors;
    animation: TThemeAnimations;
    highlight: TThemeHighlightColors;
};
export type { TTheme, TTHemeColors, TTHemeActionColors, TTHemeTextColors, TThemeBorderColors, TTHemeBackgroundColors, TThemeColorDefinition, TThemeAnimationSpeed, };
