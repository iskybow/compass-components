import type { TTheme } from './theme.types';
declare const darkTheme: TTheme;
export default darkTheme;
