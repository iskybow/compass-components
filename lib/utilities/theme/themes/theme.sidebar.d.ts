import type { TTheme } from './theme.types';
declare const sidebarTheme: TTheme;
export default sidebarTheme;
