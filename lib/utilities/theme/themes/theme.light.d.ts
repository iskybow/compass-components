import type { TTheme } from './theme.types';
declare const lightTheme: TTheme;
export default lightTheme;
