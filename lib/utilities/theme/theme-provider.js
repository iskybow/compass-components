import React from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import GlobalStyle from './global-styles';
import { lightTheme } from './themes';
const ThemeProvider = ({ children = null, theme = lightTheme }) =>
    React.createElement(
        StyledThemeProvider,
        { theme: theme },
        React.createElement(GlobalStyle, null),
        children
    );
export { StyledThemeProvider as SectionThemeProvider };
export default ThemeProvider;
