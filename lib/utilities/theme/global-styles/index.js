import { createGlobalStyle } from 'styled-components';
import { setAlpha } from '../../../shared';
import fontFaces from './font-faces';
import reset from './reset-styles';
import defaultStyles from './default-styles';
// eslint-disable-next-line import/no-unassigned-import
import '@iskybow/compass-icons/build/css/compass-icons.css';
const GlobalStyle = createGlobalStyle`
    ${({ theme }) => (theme.noStyleReset ? null : reset)};
    ${({ theme }) => (theme.noFontFaces ? null : fontFaces)};
    ${({ theme }) => (theme.noDefaultStyle ? null : defaultStyles)};

    // TODO@all: these styles need to be extracted (and adjusted) to the Skeleton component once it is ready to be built
    .skeleton {
        display: block;
        flex: 1;
        align-self: stretch;
        background: ${({ theme }) => theme.background.skeleton};
        overflow: hidden;
        position: relative;

        &::after {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            transform: translateX(-100%);
            background: linear-gradient(
                    90deg,
                    ${({ theme }) => setAlpha(theme.background.shimmer, 0)} 0,
                    ${({ theme }) => setAlpha(theme.background.shimmer, 0.25)} 40%,
                    ${({ theme }) => setAlpha(theme.background.shimmer, 0.5)} 75%,
                    ${({ theme }) => setAlpha(theme.background.shimmer, 0)}
            );
            animation: shimmer 1.5s infinite;
            content: '';
        }
    }

    @keyframes shimmer {
        100% {
            transform: translateX(100%);
        }
    }
`;
export default GlobalStyle;
