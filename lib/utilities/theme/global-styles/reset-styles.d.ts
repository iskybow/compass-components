declare const resetButton: import("styled-components").FlattenSimpleInterpolation;
declare const reset: import("styled-components").FlattenSimpleInterpolation;
export { resetButton };
export default reset;
