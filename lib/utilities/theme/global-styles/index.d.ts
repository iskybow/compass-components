import type { ThemeProps } from 'styled-components';
import type { TTheme } from '../themes';
import '@iskybow/compass-icons/build/css/compass-icons.css';
declare type PGlobalStyles = {
    theme: TTheme;
};
declare const GlobalStyle: import("styled-components").GlobalStyleComponent<ThemeProps<TTheme>, import("styled-components").DefaultTheme>;
export type { PGlobalStyles };
export default GlobalStyle;
