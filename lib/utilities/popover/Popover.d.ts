/// <reference types="react" />
import type PPopover from './Popover.props';
declare const Popover: ({ anchorReference, children, isVisible, onClickAway, noAnimation, placement, offset, zIndex, }: PPopover) => JSX.Element | null;
export default Popover;
