import { SIZES } from '../spacing';
import type { TPopoverOffset, TPopoverOffsetToken, TPopoverPlacement } from './Popover.types';
declare const POPOVER_OFFSET_TOKENS: TPopoverOffsetToken[];
declare const DEFAULT_POPOVER_OFFSET: TPopoverOffset;
declare const POPOVER_PLACEMENTS: TPopoverPlacement[];
declare const DEFAULT_POPOVER_PLACEMENT: TPopoverPlacement;
export { DEFAULT_POPOVER_OFFSET, POPOVER_OFFSET_TOKENS, SIZES as POPOVER_OFFSET_VALUES, POPOVER_PLACEMENTS, DEFAULT_POPOVER_PLACEMENT, };
