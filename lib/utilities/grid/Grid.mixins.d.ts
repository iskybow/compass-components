import type { FlattenSimpleInterpolation } from 'styled-components';
import type { PApplyGrid, PApplyGridItem } from './Grid.props';
/**
 * applies `display: grid` alongside all definitions passed to this mixin.
 *
 * parameter values are aligned with the CSS property values, so you can use
 * them as you would in normal CSS/SCSS.
 *
 * values for `gap`, `placeItems` and `placeContent` are combined, when used
 * with the CSS property values, but can be defined separately by using a object
 * notation with properties `alignItems`/`alignContent` or
 * `justifyItems`/`justifyContent`.
 *
 * @example
 * same value for `alignItems` and `justifyItems`
 * ```typescript
 * applyGridItem({
 *   placeItems: 'start'
 * })
 * ```
 *
 * @example
 * separate values for `alignItems` and `justifyItems`
 * ```typescript
 * applyGridItem({
 *   placeItems: { alignItems: 'start', justifyItems: 'stretch' }
 * })
 * ```
 *
 * @example
 * same gap value for `column` and `row`
 * ```typescript
 * applyGridItem({
 *   gap: 15
 * })
 * ```
 *
 * @example
 * separate gap values for `column` and `row`
 * ```typescript
 * applyGridItem({
 *   gap: { column: 15, row: 10 }
 * })
 * ```
 *
 * It is to be used in a components root (styled component) as shown in the
 * exmaple below:
 *
 * @example
 * usage in a components root
 * ```typescript
 * const ExampleComponentRoot = styled.div.withConfig({
 *   shouldForwardProp: (property, validator) =>
 *     Utils.blockProperty(property) && validator(property),
 * })((props: PApplyGridItem) => css`
 *   // other style definitions
 *   ${applyGrid({
 *     columnsTemplate: 'auto 1fr auto',
 *     gap: 15,
 *     placeItems: {
 *       alignItems: 'start',
 *       justifyItems: 'stretch',
 *     }
 *   })};
 * `;
 * ```
 */
declare const applyGrid: ({ columnsTemplate, rowsTemplate, areasTemplate, gap, placeItems, placeContent, }: PApplyGrid) => FlattenSimpleInterpolation;
/**
 * applies grid-item styles based on the passed values.
 *
 * parameter values are aligned with the CSS property values, so you can use
 * them as you would in normal CSS/SCSS.
 *
 * It is to be used in a components root (styled component) as shown in the
 * exmaple below:
 *
 * @example
 * usage in a components root
 * ```typescript
 * const ExampleComponentRoot = styled.div.withConfig({
 *   shouldForwardProp: (property, validator) =>
 *     Utils.blockProperty(property) && validator(property),
 * })((props: PApplyGridItem) => css`
 *   // other style definitions
 *   ${applyGridItem({
 *     columns: '1 / span 2',
 *     rows: '1',
 *   })};
 * `;
 * ```
 */
declare const applyGridItem: ({ columns, rows, placeSelf, area, }: PApplyGridItem) => FlattenSimpleInterpolation;
export { applyGrid, applyGridItem };
