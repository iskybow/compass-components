import React from 'react';
import type PGrid from './Grid.props';
import type { PGridItem } from './Grid.props';
/**
 * adding a generic Type here allows for safe typing when using elements other
 * than basic HTML (e.g. a `Shape` component)
 *
 * @example
 * ```typescript
 * <Grid<PShape>
 *   element={Shape}
 *   columnsTemplate={'auto 1fr auto'}
 *   placeItems={{alignItems: 'center'}}
 *   padding={Spacing.trbl({top: 75, right: 200, bottom: 75, left: 100})}
 *   width={'100%'}
 *   height={40}
 *   radius={0}
 * >
 * ```
 *
 * In this example neither `width`, `height` nor `radius` are properties of the
 * `Grid` component, but by using `<PShape>` we safely type those props in, when
 * using the `Shape` component in the `element` prop
 */
declare const Grid: <T>({ element, ...rest }: import("../../shared/props").PGlobalsLayout & {
    columnsTemplate?: import("csstype").Property.GridTemplateColumns<0 | (string & {})> | undefined;
    rowsTemplate?: import("csstype").Property.GridTemplateRows<0 | (string & {})> | undefined;
    areasTemplate?: import("csstype").Property.GridTemplateAreas | undefined;
    gap?: import("./Grid.types").TGridGapProperty | undefined;
    placeItems?: import("./Grid.types").TGridPlaceItemsProperty | undefined;
    placeContent?: import("./Grid.types").TGridPlaceContentProperty | undefined;
} & import("../../shared/props").PGlobalsMisc & {
    element?: string | React.FC<{}> | undefined;
} & T) => JSX.Element;
/**
 * adding a generic Type here allows for safe typing when using elements other
 * than basic HTML (e.g. a `Shape` component)
 *
 * @example
 * ```typescript
 * <GridItem<PShape>
 *   element={Shape}
 *   columns={'1 / 2'}
 *   padding={Spacing.all(75)}
 *   width={'100%'}
 *   radius={8}
 *   backgroundColor={'#FFF'}
 * >
 * ```
 *
 * In this example neither `width`, `radius` nor `backgroundColor` are
 * properties of the `GridItem` component, but by using `<PShape>` we safely
 * type those props in, when using the `Shape` component in the `element` prop
 */
declare const GridItem: <T>({ element, ...rest }: import("../../shared/props").PGlobalsLayout & {
    columns?: import("csstype").Property.GridColumn | undefined;
    rows?: import("csstype").Property.GridRow | undefined;
    placeSelf?: import("./Grid.types").TGridItemPlaceSelfProperty | undefined;
    area?: import("csstype").Property.GridArea | undefined;
} & import("../../shared/props").PGlobalsMisc & {
    element?: string | React.FC<{}> | undefined;
} & T) => JSX.Element;
export { GridItem };
export default Grid;
