var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { DEFAULT_GRID_ELEMENT, DEFAULT_GRID_ITEM_ELEMENT } from './Grid.constants';
import { GridRoot, GridItemRoot } from './Grid.root';
/**
 * adding a generic Type here allows for safe typing when using elements other
 * than basic HTML (e.g. a `Shape` component)
 *
 * @example
 * ```typescript
 * <Grid<PShape>
 *   element={Shape}
 *   columnsTemplate={'auto 1fr auto'}
 *   placeItems={{alignItems: 'center'}}
 *   padding={Spacing.trbl({top: 75, right: 200, bottom: 75, left: 100})}
 *   width={'100%'}
 *   height={40}
 *   radius={0}
 * >
 * ```
 *
 * In this example neither `width`, `height` nor `radius` are properties of the
 * `Grid` component, but by using `<PShape>` we safely type those props in, when
 * using the `Shape` component in the `element` prop
 */
const Grid = function (_a) {
    var { element = DEFAULT_GRID_ELEMENT } = _a,
        rest = __rest(_a, ['element']);
    return React.createElement(GridRoot, Object.assign({}, rest, { as: element }));
};
/**
 * adding a generic Type here allows for safe typing when using elements other
 * than basic HTML (e.g. a `Shape` component)
 *
 * @example
 * ```typescript
 * <GridItem<PShape>
 *   element={Shape}
 *   columns={'1 / 2'}
 *   padding={Spacing.all(75)}
 *   width={'100%'}
 *   radius={8}
 *   backgroundColor={'#FFF'}
 * >
 * ```
 *
 * In this example neither `width`, `radius` nor `backgroundColor` are
 * properties of the `GridItem` component, but by using `<PShape>` we safely
 * type those props in, when using the `Shape` component in the `element` prop
 */
const GridItem = function (_a) {
    var { element = DEFAULT_GRID_ITEM_ELEMENT } = _a,
        rest = __rest(_a, ['element']);
    return React.createElement(GridItemRoot, Object.assign({}, rest, { as: element }));
};
export { GridItem };
export default Grid;
