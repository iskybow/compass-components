import type { Property } from 'csstype';
declare type TGridPlaceItemsProperty = Property.PlaceItems | {
    alignItems?: Property.AlignItems;
    justifyItems?: Property.JustifyItems;
};
declare type TGridPlaceContentProperty = Property.PlaceContent | {
    alignContent?: Property.AlignContent;
    justifyContent?: Property.JustifyContent;
};
declare type TGridGapProperty = Property.Gap | {
    column?: Property.ColumnGap;
    row?: Property.RowGap;
};
declare type TGridItemPlaceSelfProperty = Property.PlaceSelf | {
    alignItems?: Property.AlignSelf;
    justifyItems?: Property.JustifySelf;
};
export type { TGridPlaceItemsProperty, TGridPlaceContentProperty, TGridGapProperty, TGridItemPlaceSelfProperty, };
