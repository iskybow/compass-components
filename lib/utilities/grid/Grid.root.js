var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import styled, { css } from 'styled-components';
import { Utils } from '../../shared';
import { applyMargin, applyPadding } from '../spacing';
import { applyGrid, applyGridItem } from './Grid.mixins';
const GridRoot = styled.div.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property, [
            'columnsTemplate',
            'rowsTemplate',
            'areasTemplate',
            'gap',
            'placeItems',
            'placeContent',
        ]) && validator(property),
})((_a) => {
    var { padding, margin } = _a,
        rest = __rest(_a, ['padding', 'margin']);
    return css`
        ${applyGrid(rest)};
        ${padding && applyPadding(padding)};
        ${margin && applyMargin(margin)};
    `;
});
const GridItemRoot = styled.div.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property, ['columns', 'rows', 'area']) && validator(property),
})((_a) => {
    var { padding, margin } = _a,
        rest = __rest(_a, ['padding', 'margin']);
    return css`
        ${applyGridItem(rest)};
        ${padding && applyPadding(padding)};
        ${margin && applyMargin(margin)};
    `;
});
export { GridRoot, GridItemRoot };
