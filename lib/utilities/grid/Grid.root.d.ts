declare const GridRoot: import("styled-components").StyledComponent<"div", any, import("../../shared/props").PGlobalsLayout & {
    columnsTemplate?: import("csstype").Property.GridTemplateColumns<0 | (string & {})> | undefined;
    rowsTemplate?: import("csstype").Property.GridTemplateRows<0 | (string & {})> | undefined;
    areasTemplate?: import("csstype").Property.GridTemplateAreas | undefined;
    gap?: import("./Grid.types").TGridGapProperty | undefined;
    placeItems?: import("./Grid.types").TGridPlaceItemsProperty | undefined;
    placeContent?: import("./Grid.types").TGridPlaceContentProperty | undefined;
}, never>;
declare const GridItemRoot: import("styled-components").StyledComponent<"div", any, import("../../shared/props").PGlobalsLayout & {
    columns?: import("csstype").Property.GridColumn | undefined;
    rows?: import("csstype").Property.GridRow | undefined;
    placeSelf?: import("./Grid.types").TGridItemPlaceSelfProperty | undefined;
    area?: import("csstype").Property.GridArea | undefined;
}, never>;
export { GridRoot, GridItemRoot };
