import { Utils } from '../../shared';
import { SIZES } from './Spacing.constants';
const Spacing = {
    all(spacingToken) {
        return [spacingToken, spacingToken, spacingToken, spacingToken];
    },
    trbl({ top = 0, right = 0, bottom = 0, left = 0 }) {
        return [top, right, bottom, left];
    },
    only(side, spacingToken) {
        const trbl = {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
        };
        trbl[side] = spacingToken;
        return this.trbl(trbl);
    },
    symmetric({ vertical = 0, horizontal = 0 }) {
        return this.trbl({
            top: vertical,
            right: horizontal,
            bottom: vertical,
            left: horizontal,
        });
    },
};
const parseSpacing = (spacing) =>
    spacing.map((s) => (s ? Utils.getPxValue(SIZES[s]) : 0)).join(' ');
export { parseSpacing };
export default Spacing;
