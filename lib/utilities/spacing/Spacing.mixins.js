import { css } from 'styled-components';
import { parseSpacing } from './Spacing';
function applyPadding(spacing) {
    return css`
        padding: ${parseSpacing(spacing)};
    `;
}
function applyMargin(spacing) {
    return css`
        margin: ${parseSpacing(spacing)};
    `;
}
export { applyPadding, applyMargin };
