import type { TSpacingToken } from './Spacing.types';
declare const SIZES: Record<TSpacingToken, number>;
declare const SPACING_TOKENS: TSpacingToken[];
export { SIZES, SPACING_TOKENS };
export default SIZES;
