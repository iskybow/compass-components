declare type TSpacingToken = 0 | 25 | 50 | 75 | 100 | 125 | 150 | 175 | 200 | 250 | 300 | 350 | 400 | 450 | 500 | 600 | 700 | 800;
declare type TSpacingSides = 'top' | 'right' | 'bottom' | 'left';
declare type TSpacingSymmetricSides = 'vertical' | 'horizontal';
declare type TSpacingTokensTRBL = {
    [key in TSpacingSides]?: TSpacingToken;
};
declare type TSpacingTokensSymmetric = {
    [key in TSpacingSymmetricSides]?: TSpacingToken;
};
declare type TSpacingDefinition = [
    TSpacingToken | null,
    TSpacingToken | null,
    TSpacingToken | null,
    TSpacingToken | null
];
declare type TSpacing = {
    all: (spacingToken: TSpacingToken) => TSpacingDefinition;
    trbl: (spacingTokensTRBL: TSpacingTokensTRBL) => TSpacingDefinition;
    only: (side: TSpacingSides, spacingToken: TSpacingToken) => TSpacingDefinition;
    symmetric: (spacingTokensSymmetric: TSpacingTokensSymmetric) => TSpacingDefinition;
};
export type { TSpacing, TSpacingSides, TSpacingToken, TSpacingTokensTRBL, TSpacingDefinition, TSpacingTokensSymmetric, };
