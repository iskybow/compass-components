import type { TSpacing, TSpacingDefinition } from './Spacing.types';
declare const Spacing: TSpacing;
declare const parseSpacing: (spacing: TSpacingDefinition) => string;
export { parseSpacing };
export default Spacing;
