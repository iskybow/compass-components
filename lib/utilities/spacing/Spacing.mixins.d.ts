import type { FlattenSimpleInterpolation } from 'styled-components';
import type { TSpacingDefinition } from './Spacing.types';
declare function applyPadding(spacing: TSpacingDefinition): FlattenSimpleInterpolation;
declare function applyMargin(spacing: TSpacingDefinition): FlattenSimpleInterpolation;
export { applyPadding, applyMargin };
