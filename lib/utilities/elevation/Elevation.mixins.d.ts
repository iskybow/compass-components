import type { FlattenSimpleInterpolation } from 'styled-components';
import type PElevation from './Elevation.props';
/**
 * Apply elevation styles to a styled component.
 * When the elevationOnHover is not the same as the elevation value
 * it will create hover styles as well.
 *
 * @param {number} elevation
 * @param {number} elevationOnHover
 * @param {boolean} darkTheme
 * @returns {FlattenSimpleInterpolation}
 */
declare function applyElevation({ elevation, elevationOnHover }: PElevation, darkTheme: boolean): FlattenSimpleInterpolation | null;
export default applyElevation;
