declare type TElevationLevel = 0 | 1 | 2 | 3 | 4 | 5 | 6;
declare type TElevationDefinition = {
    offsetY: number;
    blurRadius: number;
};
declare type TElevationDefinitions = {
    [key in TElevationLevel]: TElevationDefinition;
};
export type { TElevationLevel, TElevationDefinition, TElevationDefinitions };
