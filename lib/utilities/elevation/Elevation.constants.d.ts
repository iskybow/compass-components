import type { TElevationLevel, TElevationDefinitions } from './Elevation.types';
declare const ELEVATION_LEVELS: TElevationLevel[];
declare const DEFAULT_ELEVATION_LEVEL: TElevationLevel;
declare const ELEVATION_DEFINITIONS: TElevationDefinitions;
export { ELEVATION_LEVELS, DEFAULT_ELEVATION_LEVEL, ELEVATION_DEFINITIONS };
