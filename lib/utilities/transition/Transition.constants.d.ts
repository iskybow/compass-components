import type { TThemeAnimationSpeed } from '../theme';
import type { TTransitionTypeStatusMap } from './Transition.types';
declare const TRANSITION_SPEEDS: TThemeAnimationSpeed[];
declare const DEFAULT_TRANSITION_SPEED: TThemeAnimationSpeed;
declare const TRANSITION_TYPE_DEFINITIONS: TTransitionTypeStatusMap;
export { TRANSITION_SPEEDS, DEFAULT_TRANSITION_SPEED, TRANSITION_TYPE_DEFINITIONS };
