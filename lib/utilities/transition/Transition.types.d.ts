import type { TransitionStatus } from 'react-transition-group';
import type { FlattenSimpleInterpolation } from 'styled-components';
import type { TThemeAnimationSpeed } from '../theme';
declare type TTransitionType = 'fade' | 'scale';
declare type TTransitionTypeDefinition = {
    [key in TransitionStatus]: FlattenSimpleInterpolation;
} & {
    properties: string[];
};
declare type TTransitionTypeStatusMap = {
    [key in TTransitionType]: TTransitionTypeDefinition;
} & {
    custom?: TTransitionTypeDefinition;
};
export type { TTransitionType, TTransitionTypeStatusMap, TTransitionTypeDefinition, TThemeAnimationSpeed as TTransitionSpeed, };
