import React from 'react';
declare const _default: React.ForwardRefExoticComponent<{
    style?: React.CSSProperties | undefined;
    type?: import("./Transition.types").TTransitionType | import("./Transition.types").TTransitionType[] | undefined;
    children: React.ReactNode;
    onTransitionEnd?: (() => void) | undefined;
    speed?: import("../theme/themes/theme.types").TThemeAnimationSpeed | {
        in?: import("../theme/themes/theme.types").TThemeAnimationSpeed | undefined;
        out?: import("../theme/themes/theme.types").TThemeAnimationSpeed | undefined;
    } | undefined;
    enter?: boolean | undefined;
    customTransition?: import("./Transition.types").TTransitionTypeDefinition | undefined;
    delay?: number | {
        in?: number | undefined;
        out?: number | undefined;
    } | undefined;
    isVisible: boolean;
    exit?: boolean | undefined;
    onEnter?: ((node: HTMLElement, isAppearing: boolean) => void) | undefined;
    onEntering?: ((node: HTMLElement, isAppearing: boolean) => void) | undefined;
    onEntered?: ((node: HTMLElement, isAppearing: boolean) => void) | undefined;
    onExit?: ((node: HTMLElement) => void) | undefined;
    onExiting?: ((node: HTMLElement) => void) | undefined;
    onExited?: ((node: HTMLElement) => void) | undefined;
    mountOnEnter?: boolean | undefined;
    unmountOnExit?: boolean | undefined;
} & {
    theme?: any;
}>;
export default _default;
