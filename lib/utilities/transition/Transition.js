var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Transition as TransitionRoot } from 'react-transition-group';
import styled, { css, withTheme } from 'styled-components';
import { Utils } from '../../shared';
import { DEFAULT_TRANSITION_SPEED, TRANSITION_TYPE_DEFINITIONS } from './Transition.constants';
const Animation = styled.div(({ duration, state, types, delay, customTransition }) => {
    const customProperties =
        (customTransition === null || customTransition === void 0
            ? void 0
            : customTransition.properties) || [];
    const properties = Array.from(
        new Set(
            types
                .flatMap((type) => TRANSITION_TYPE_DEFINITIONS[type].properties)
                .concat(customProperties)
        )
    );
    const customTransitionStyles =
        customTransition && customTransition[state]
            ? css`
                  ${customTransition[state]}
              `
            : null;
    return css`
        transition: ${properties
            .map((property) => `${property} ${duration}ms ease-in-out ${delay}ms`)
            .join(', ')};
        ${types.map((type) => TRANSITION_TYPE_DEFINITIONS[type][state])};
        ${customTransitionStyles};
    `;
});
const Transition = (_a) => {
    var {
            isVisible,
            children,
            theme,
            type = [],
            enter = true,
            exit = true,
            speed = {
                in: DEFAULT_TRANSITION_SPEED,
                out: DEFAULT_TRANSITION_SPEED,
            },
            delay = {
                in: 0,
                out: 0,
            },
            onTransitionEnd = Utils.noop,
            customTransition,
        } = _a,
        rest = __rest(_a, [
            'isVisible',
            'children',
            'theme',
            'type',
            'enter',
            'exit',
            'speed',
            'delay',
            'onTransitionEnd',
            'customTransition',
        ]);
    const types = Array.isArray(type) ? type : [type];
    const delays = {
        in: Utils.isNumber(delay) ? delay : delay.in,
        out: Utils.isNumber(delay) ? delay : delay.out,
    };
    const speeds = {
        in: Utils.isString(speed) ? theme.animation[speed] : theme.animation[speed.in || 'normal'],
        out: Utils.isString(speed)
            ? theme.animation[speed]
            : theme.animation[speed.out || 'normal'],
    };
    const rootProperties = Object.assign(
        { in: isVisible, addEndListener: onTransitionEnd, enter, exit },
        rest
    );
    return React.createElement(TransitionRoot, Object.assign({}, rootProperties), (state) =>
        React.createElement(
            Animation,
            {
                state: state,
                types: types,
                duration: state === 'entering' || state === 'entered' ? speeds.in : speeds.out,
                customTransition: customTransition,
                delay: state === 'entering' || state === 'entered' ? delays.in : delays.out,
            },
            children
        )
    );
};
export default withTheme(Transition);
