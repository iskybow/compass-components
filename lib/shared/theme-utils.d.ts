import type { TBaseColorShade } from '../foundations/colors/colors.types';
import type { TThemeColorDefinition } from '../utilities/theme';
import { getContrastRatio } from './color-utils';
declare function createThemeColor(color: string, shade?: TBaseColorShade): TThemeColorDefinition;
export { createThemeColor, getContrastRatio };
