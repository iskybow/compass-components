import type { THiddenArgtypes } from './types';
/**
 * pass in a story(Parameters) and get the correct URL to directly link to it
 * @param {object} storyParameters
 */
declare function getStoryDocumentationUrl(storyParameters: Record<string, string>): string;
/**
 * hide the components listed in the blacklist array
 * @param {string[]} [blacklist]
 * @returns {THiddenArgtypes}
 */
declare function hideComponentProperties(blacklist?: string[]): THiddenArgtypes;
declare function warn(message: string, ...rest: any): void;
/**
 * check if a given string is a valid color value for CSS
 * @param {string} colorString
 */
declare function isColor(colorString: string): boolean;
/**
 * Returns a number whose value is limited to the given range.
 * @param {number} value The value to be clamped
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} A number in the range [min, max]
 */
declare function clamp(value: number, min?: number, max?: number): number;
/**
 * will return a base64 value from a image endpoint or image source
 * @param {string} url
 */
declare function getBase64(url: string): Promise<string>;
/**
 * custom error class to throw for compass components
 */
declare class CompassError extends Error {
    constructor(message: string);
}
/**
 * Asserts if a certain check is true. If not throw a CompassError with the
 * provided message or warn in the console.
 *
 * When in production it will never throw, but instead warn the user.
 * @param {boolean} assertion
 * @param {string} message
 * @param {boolean} warnOnly
 */
declare function assert(assertion: boolean, message: string, warnOnly?: boolean): void;
/**
 * This LITERALLY does NOTHING! :D
 * @returns {void}
 */
declare function noop(): void;
declare const Utils: {
    warn: typeof warn;
    assert: typeof assert;
    clamp: typeof clamp;
    isColor: typeof isColor;
    isNumber: (x: any) => x is number;
    isFunction: (x: any) => x is Function;
    isString: (x: any) => x is string;
    isFunctionalComponent: (component: any) => boolean;
    blockProperty: (property: string | number | symbol, blackList?: (string | number | symbol)[]) => boolean;
    forceForwardProperty: (property: string | number | symbol, whitelist?: (string | number | symbol)[]) => boolean;
    getBase64: typeof getBase64;
    getStoryDocumentationUrl: typeof getStoryDocumentationUrl;
    hideComponentProperties: typeof hideComponentProperties;
    getFontMargin: (fontSize: number, multiplier: number) => number;
    getPxValue: (value: string | number) => string;
    noop: typeof noop;
};
export { CompassError };
export default Utils;
