import type { TBaseColorShade } from '../foundations/colors/colors.types';
declare type TColorDefinition = {
    type: string;
    values: number[];
};
/**
 * Converts a color from CSS hex format to CSS rgb format.
 * @param {string} color - Hex color, i.e. #nnn or #nnnnnn
 * @returns {string} A CSS rgb color string
 */
declare function hexToRgb(color: string): string;
/**
 * Converts a color from CSS rgb format to CSS hex format.
 * @param {string} color - RGB color, i.e. rgb(n, n, n)
 * @returns {string} A CSS rgb color string, i.e. #nnnnnn
 */
declare function rgbToHex(color: string): string;
/**
 * Converts a color from CSS rgb format to CSS hsl format.
 * @param {string} color - RGB color, i.e. rgb(n, n, n)
 * @returns {string} A CSS rgb color string, i.e. #nnnnnn
 */
declare function rgbToHsl(color: string): string;
/**
 * Converts a color from hsl format to rgb format.
 * @param {string} color - HSL color values
 * @returns {string} rgb color values
 */
declare function hslToRgb(color: string): string;
/**
 * Returns an object with the typography and values of a color.
 *
 * Note: Does not support rgb % values.
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {object} - A MUI color object: {typography: string, values: number[]}
 */
declare function decomposeColor(color: string): TColorDefinition;
/**
 * Converts a color object with typography and values to a string.
 * @param {object} color - Decomposed color
 * @param {string} color.typography - One of: 'rgb', 'rgba', 'hsl', 'hsla'
 * @param {array} color.values - [n,n,n] or [n,n,n,n]
 * @returns {string} A CSS color string
 */
declare function recomposeColor(color: TColorDefinition): string;
/**
 * Converts a color object with typography and values to a string.
 * @param {object} color - Decomposed color
 * @param {string} color.typography - One of: 'rgb', 'rgba', 'hsl', 'hsla'
 * @param {array} color.values - [n,n,n] or [n,n,n,n]
 * @param {number} shade - 100 | 200 | ... | 800
 * @returns {string} A CSS color string
 */
declare function recomposeColorWithShade(color: TColorDefinition, shade: TBaseColorShade): string;
/**
 * Create a full map of all shades for a given color
 * @param {string} color
 * @returns {Record<string, string>}
 */
declare function createColorShades(color: string): Record<string, string>;
/**
 * Converts any given valid and supported color string to rgb
 * @param {string} color - color string
 * @returns {string} rgb color string
 */
declare function convertToRgb(color: string): string;
/**
 * Set the absolute transparency of a color.
 * Any existing alpha values are overwritten.
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} value - value to set the alpha channel to in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */
declare function setAlpha(color: string, value: number): string;
/**
 * Calculates the contrast ratio between two colors.
 *
 * Formula: https://www.w3.org/TR/WCAG20-TECHS/G17.html#G17-tests
 * @param {string} foreground - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {string} background - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {number} A contrast ratio value in the range 0 - 21.
 */
declare function getContrastRatio(foreground: string, background: string): number;
/**
 * Darkens a color.
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */
declare function darken(color: string, coefficient: number): string;
/**
 * Lightens a color.
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */
declare function lighten(color: string, coefficient: number): string;
/**
 * Blends to colors in normal mode
 * @param {string} background - the background color
 * @param {string} foreground - the foreground color layered on top
 * @returns {string} rgb color string
 */
declare function blendColors(background: string, foreground: string): string;
export { setAlpha, convertToRgb, decomposeColor, recomposeColor, recomposeColorWithShade, hexToRgb, hslToRgb, rgbToHex, rgbToHsl, createColorShades, blendColors, darken, lighten, getContrastRatio, };
