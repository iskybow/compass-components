const FONT_TYPE_FAMILIES = {
    body: 'Open Sans, sans-serif',
    heading: 'Metropolis, sans-serif',
};
const FONT_WEIGHTS = ['light', 'regular', 'bold'];
const FONT_WEIGHT_MAP = {
    light: 300,
    regular: 400,
    bold: 600,
};
const FONT_MARGINS = ['none', 'both', 'bottom', 'top'];
const FONT_COLORS = ['primary', 'secondary', 'disabled', 'inherit'];
const DEFAULT_ARGUMENTS_TABLE_EXCLUSION = ['children'];
const DEFAULT_PROPERTY_WHITELIST = [
    'children',
    'className',
    'disabled',
    'role',
    'selected',
    'type',
    'onClick',
    'checked',
    'defaultChecked',
];
const COMPONENT_SIZES = ['xxxs', 'xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'xxl', 'xxxl'];
export {
    COMPONENT_SIZES,
    DEFAULT_ARGUMENTS_TABLE_EXCLUSION,
    DEFAULT_PROPERTY_WHITELIST,
    FONT_TYPE_FAMILIES,
    FONT_COLORS,
    FONT_MARGINS,
    FONT_WEIGHTS,
    FONT_WEIGHT_MAP,
};
