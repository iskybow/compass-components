var __awaiter =
    (this && this.__awaiter) ||
    function (thisArg, _arguments, P, generator) {
        function adopt(value) {
            return value instanceof P
                ? value
                : new P(function (resolve) {
                      resolve(value);
                  });
        }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) {
                try {
                    step(generator.next(value));
                } catch (e) {
                    reject(e);
                }
            }
            function rejected(value) {
                try {
                    step(generator['throw'](value));
                } catch (e) {
                    reject(e);
                }
            }
            function step(result) {
                result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
            }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
import kebabCase from 'lodash.kebabcase';
import { DEFAULT_PROPERTY_WHITELIST } from './constants';
/**
 * pass in a story(Parameters) and get the correct URL to directly link to it
 * @param {object} storyParameters
 */
function getStoryDocumentationUrl(storyParameters) {
    const storyPathParts = storyParameters.title.split('/');
    const storyPath = storyPathParts.map((part) => kebabCase(part)).join('-');
    if (storyParameters.includeStories.length === 0) {
        return `/?path=/docs/${storyPath}--page`;
    }
    return `/?path=/docs/${storyPath}--${kebabCase(storyParameters.includeStories[0])}`;
}
/**
 * this is to prevent all properties to be passed down to the underlying
 * component, except for the ones we want to. (e.g. `type="button"`)
 * Leave the blackList empty or do not pass a value to allow all properties
 * to be passed dow.
 *
 * `data-*` and `aria-*` attributes are always passed down.
 *
 * It is to be used in the styled components `shouldForwardProp` config
 *
 * @example
 * ```typescript
 * // pass down `width` and `height` properties
 * const StyledDiv = styled.div.withConfig({
 *   shouldForwardProp: (property, validator) =>
 *     Utils.blockProperty(property, ['width', 'height']) && validator(property),
 * })<PDiv>` ... `
 *
 * // allow all properties to be passed down
 * const StyledSection = styled.section.withConfig({
 *   shouldForwardProp: (property, validator) =>
 *     Utils.blockProperty(property) && validator(property),
 * })<PSection>` ... `
 * ```
 * */
const blockProperty = (property, blackList = []) =>
    // forward the property when it is a `data-*`attribute
    property.toString().startsWith('data-') ||
    // forward the property when it is a `aria-*`attribute
    property.toString().startsWith('aria-') ||
    // always forward the property when it is defined within the property-whitelist
    DEFAULT_PROPERTY_WHITELIST.includes(property.toString()) ||
    // forward the property when it is defined within the passed property-whitelist
    !blackList.includes(property.toString());
/**
 * this function "force-forwards" a property.
 * it is especially helpful when you need to pass properties from an extended
 * root component to the component it inherits from
 *
 * in this example we want to force-forward (pass) properties to the
 * Icon component ButtonIconRoot is extending from, but since the default
 * validator function from styled-components does not handle 'glyph' and 'size'
 * as standard HTML attributes it blocks them fropm being passed on
 *
 * @example
 * ```typescript
 * const ButtonIconRoot = styled(Icon).withConfig<PButtonIconRoot>({
 *   shouldForwardProp: (property, validator) =>
 *       Utils.forceForwardProperty(property, ['glyph', 'size']) ||
 *       (Utils.blockProperty(property, ['width']) && validator(property)),
 * })(
 *   ({ margin, marginPosition }) => css`
 *       margin-${marginPosition}: ${margin}px;
 *     `
 * );
 * ```
 */
const forceForwardProperty = (property, whitelist = []) => whitelist.includes(property.toString());
/**
 * hide the properties that come with the styled component API
 * @returns {THiddenArgtypes}
 */
function hideStyledComponentProperties() {
    return {
        forwardedAs: { table: { disable: true } },
        theme: { table: { disable: true } },
        ref: { table: { disable: true } },
        as: { table: { disable: true } },
    };
}
/**
 * hide the components listed in the blacklist array
 * @param {string[]} [blacklist]
 * @returns {THiddenArgtypes}
 */
function hideComponentProperties(blacklist = []) {
    return blacklist.reduce(
        (o, key) => Object.assign(o, { [key]: { table: { disable: true } } }),
        hideStyledComponentProperties()
    );
}
/* eslint-disable @typescript-eslint/no-explicit-any,no-console */
function warn(message, ...rest) {
    console.warn(message, ...rest);
}
const isNumber = (x) => typeof x === 'number';
const isString = (x) => typeof x === 'string';
const isFunction = (x) => typeof x === 'function';
/**
 * check if the provided component is classified as functional component
 * @param {*} component
 * @returns {boolean}
 */
const isFunctionalComponent = (component) =>
    isFunction(component) && !(component.prototype && component.prototype.isReactComponent);
/* eslint-enable @typescript-eslint/no-explicit-any,no-console */
/**
 * check if a given string is a valid color value for CSS
 * @param {string} colorString
 */
function isColor(colorString) {
    const s = new Option().style;
    s.color = colorString;
    return s.color === colorString;
}
const getFontMargin = (fontSize, multiplier) =>
    Math.max(Math.round((fontSize * multiplier) / 4) * 4, 8);
const getPxValue = (value) => (isNumber(value) ? `${value}px` : value);
/**
 * Returns a number whose value is limited to the given range.
 * @param {number} value The value to be clamped
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} A number in the range [min, max]
 */
function clamp(value, min = 0, max = 1) {
    if (value < min || value > max) {
        throw new Error(
            `Compass Components: The value provided ${value} is out of range [${min}, ${max}].`
        );
    }
    return Math.min(Math.max(min, value), max);
}
/**
 * will return a base64 value from a image endpoint or image source
 * @param {string} url
 */
function getBase64(url) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield fetch(url);
        const buf = yield response.arrayBuffer();
        const dataString = Buffer.from(buf).toString('base64');
        const dataType = response.headers.get('content-type');
        return `data:${dataType};base64,${dataString}`;
    });
}
/**
 * custom error class to throw for compass components
 */
class CompassError extends Error {
    constructor(message) {
        super(message);
        this.name = 'CompassError';
    }
}
/**
 * Asserts if a certain check is true. If not throw a CompassError with the
 * provided message or warn in the console.
 *
 * When in production it will never throw, but instead warn the user.
 * @param {boolean} assertion
 * @param {string} message
 * @param {boolean} warnOnly
 */
function assert(assertion, message, warnOnly = process.env.NODE_ENV !== 'production') {
    if (!assertion) {
        if (warnOnly) {
            // eslint-disable-next-line no-console
            console.warn(`Compass Components - ${message}`);
            return;
        }
        throw new CompassError(message);
    }
}
/**
 * This LITERALLY does NOTHING! :D
 * @returns {void}
 */
function noop() {}
const Utils = {
    warn,
    assert,
    clamp,
    isColor,
    isNumber,
    isFunction,
    isString,
    isFunctionalComponent,
    blockProperty,
    forceForwardProperty,
    getBase64,
    getStoryDocumentationUrl,
    hideComponentProperties,
    getFontMargin,
    getPxValue,
    noop,
};
export { CompassError };
export default Utils;
