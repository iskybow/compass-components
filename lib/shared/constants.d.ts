import type { TComponentSizes, TFontColor, TFontMargin, TFontWeight } from './types';
declare const FONT_TYPE_FAMILIES: Record<'heading' | 'body', string>;
declare const FONT_WEIGHTS: TFontWeight[];
declare const FONT_WEIGHT_MAP: Record<TFontWeight, number>;
declare const FONT_MARGINS: TFontMargin[];
declare const FONT_COLORS: TFontColor[];
declare const DEFAULT_ARGUMENTS_TABLE_EXCLUSION: string[];
declare const DEFAULT_PROPERTY_WHITELIST: string[];
declare const COMPONENT_SIZES: TComponentSizes;
export { COMPONENT_SIZES, DEFAULT_ARGUMENTS_TABLE_EXCLUSION, DEFAULT_PROPERTY_WHITELIST, FONT_TYPE_FAMILIES, FONT_COLORS, FONT_MARGINS, FONT_WEIGHTS, FONT_WEIGHT_MAP, };
