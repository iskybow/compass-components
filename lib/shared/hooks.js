import { useEffect } from 'react';
const EVENTS = ['mousedown', 'touchstart'];
export const useClickAway = (references, onClickAway) => {
    useEffect(() => {
        const handler = (event) => {
            if (onClickAway) {
                const isClickAway = references.every((reference) => {
                    const { current: element } = reference;
                    return element && !element.contains(event.target);
                });
                if (isClickAway) {
                    onClickAway(event);
                }
            }
        };
        if (onClickAway) {
            for (const eventName of EVENTS) {
                document.addEventListener(eventName, handler);
            }
        }
        return () => {
            if (onClickAway) {
                for (const eventName of EVENTS) {
                    document.removeEventListener(eventName, handler);
                }
            }
        };
    }, [onClickAway, references]);
};
