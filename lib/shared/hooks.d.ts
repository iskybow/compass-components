import type { RefObject } from 'react';
declare type TonClickAwayCallback = (event: Event) => void;
declare type TuseClickAwayReferences = Array<RefObject<HTMLElement | null>>;
export declare const useClickAway: (references: TuseClickAwayReferences, onClickAway?: TonClickAwayCallback | undefined) => void;
export type { TonClickAwayCallback, TuseClickAwayReferences };
