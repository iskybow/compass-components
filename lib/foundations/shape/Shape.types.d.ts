import type React from 'react';
import type { TContainerElement, TInteractionElement } from '../../shared';
declare type TShapeElement = TContainerElement | TInteractionElement | React.FC;
declare type TShapeVariant = 'rectangle' | 'circle' | 'pill';
declare type TShapeBorderRadius = 0 | 2 | 4 | 8 | 12 | 16 | 20 | 24 | 'circle' | 'pill';
export type { TShapeVariant, TShapeElement, TShapeBorderRadius };
