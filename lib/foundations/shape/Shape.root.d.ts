import type { TTheme } from '../../utilities/theme';
import type { PShapeRoot } from './Shape.props';
declare const ShapeRoot: import("styled-components").StyledComponent<"div", any, PShapeRoot & import("styled-components").ThemeProps<TTheme>, never>;
export default ShapeRoot;
