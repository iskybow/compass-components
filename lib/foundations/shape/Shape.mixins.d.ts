import type { FlattenSimpleInterpolation } from 'styled-components';
import type { PApplyShape } from './Shape.props';
/**
 * apply a shape to a given element without the use of an additional wrapping Shape component.
 *
 * @param {(number|string)?} width
 * @param {(number|string)?} height
 * @param {TShapeBorderRadius} radius
 * @returns {FlattenSimpleInterpolation}
 */
declare function applyShape({ width, height, radius, }: PApplyShape): FlattenSimpleInterpolation;
export default applyShape;
