/// <reference types="react" />
import type PShape from './Shape.props';
declare const Shape: (props: PShape) => JSX.Element;
export default Shape;
