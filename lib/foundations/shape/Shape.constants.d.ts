import { DEFAULT_ELEVATION_LEVEL } from '../../utilities/elevation';
import type { TShapeBorderRadius, TShapeElement } from './Shape.types';
declare const SHAPE_BORDER_RADII: TShapeBorderRadius[];
declare const DEFAULT_SHAPE_BORDER_RADIUS: TShapeBorderRadius;
declare const SHAPE_ELEMENTS: TShapeElement[];
declare const DEFAULT_SHAPE_ELEMENT: TShapeElement;
export { DEFAULT_ELEVATION_LEVEL as DEFAULT_SHAPE_ELEVATION_LEVEL, SHAPE_BORDER_RADII, DEFAULT_SHAPE_BORDER_RADIUS, SHAPE_ELEMENTS, DEFAULT_SHAPE_ELEMENT, };
