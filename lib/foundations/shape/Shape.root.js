import styled, { css } from 'styled-components';
import applyElevation from '../../utilities/elevation';
import { applyMargin, applyPadding } from '../../utilities/spacing';
import { Utils } from '../../shared';
import applyShape from './Shape.mixins';
const ShapeRoot = styled.div.withConfig({
    shouldForwardProp: (property, validator) =>
        Utils.blockProperty(property, ['width', 'height', 'radius', 'elevation', 'element']) &&
        validator(property),
})((props) => {
    const {
        radius,
        elevation,
        elevationOnHover,
        width,
        height,
        theme,
        padding,
        margin,
        backgroundColor = theme.background.shape,
    } = props;
    return css`
        display: flex;
        background-color: ${backgroundColor};

        ${applyShape({ width, height, radius })};
        ${applyElevation({ elevation, elevationOnHover }, theme.type === 'dark')};

        ${padding && applyPadding(padding)};
        ${margin && applyMargin(margin)};
    `;
});
export default ShapeRoot;
