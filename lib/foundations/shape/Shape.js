var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import ShapeRoot from './Shape.root';
import {
    DEFAULT_SHAPE_ELEVATION_LEVEL,
    DEFAULT_SHAPE_BORDER_RADIUS,
    DEFAULT_SHAPE_ELEMENT,
    SHAPE_ELEMENTS,
} from './Shape.constants';
const Shape = (props) => {
    const {
            element = DEFAULT_SHAPE_ELEMENT,
            radius = DEFAULT_SHAPE_BORDER_RADIUS,
            elevation = DEFAULT_SHAPE_ELEVATION_LEVEL,
            elevationOnHover,
        } = props,
        rest = __rest(props, ['element', 'radius', 'elevation', 'elevationOnHover']);
    Utils.assert(
        SHAPE_ELEMENTS.includes(element) || Utils.isFunctionalComponent(element),
        `Shape: used element ${element} is unsupported. Please use one supported by the component from this list: ${SHAPE_ELEMENTS.join(
            ', '
        )}`,
        true
    );
    const rootProperties = {
        radius,
        elevation,
        elevationOnHover,
    };
    return React.createElement(ShapeRoot, Object.assign({ as: element }, rootProperties, rest));
};
export default Shape;
