/// <reference types="react" />
import type PIcon from './Icon.props';
declare const Icon: (props: PIcon) => JSX.Element;
export default Icon;
