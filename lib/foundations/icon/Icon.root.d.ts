import type { TTheme } from '../../utilities/theme';
declare const IconRoot: import("styled-components").StyledComponent<"i", any, Required<Pick<import("./Icon.props").default, "size" | "color" | "glyph">> & Pick<import("./Icon.props").default, "className" | "ariaLabel"> & import("styled-components").ThemeProps<TTheme>, never>;
export default IconRoot;
