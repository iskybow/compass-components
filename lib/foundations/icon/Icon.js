var __rest =
    (this && this.__rest) ||
    function (s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === 'function')
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
import React from 'react';
import { Utils } from '../../shared';
import IconRoot from './Icon.root';
import {
    DEFAULT_ICON_COLOR,
    DEFAULT_ICON_GLYPH,
    DEFAULT_ICON_SIZE,
    ICON_GLYPHS,
} from './Icon.constants';
const Icon = (props) => {
    const {
            glyph = DEFAULT_ICON_GLYPH,
            size = DEFAULT_ICON_SIZE,
            color = DEFAULT_ICON_COLOR,
            className = '',
        } = props,
        rest = __rest(props, ['glyph', 'size', 'color', 'className']);
    Utils.assert(
        ICON_GLYPHS.includes(glyph),
        `Icon: please provide a valid option for the \`iconGlyph\` property. Choose from the following: ${ICON_GLYPHS.join(
            ', '
        )}.`
    );
    const rootProperties = {
        size,
        color,
        glyph,
        className: `${className} icon-${glyph}`,
    };
    return React.createElement(IconRoot, Object.assign({}, rootProperties, rest));
};
export default Icon;
