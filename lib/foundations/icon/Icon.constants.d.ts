import type { TIconColor, TIconGlyph, TIconSize } from './Icon.types';
declare const DEFAULT_ICON_SIZE = 20;
declare const ICON_SIZES: TIconSize[];
declare const ICON_FONT_SIZES: Record<TIconSize, number>;
declare const ICON_GLYPHS: TIconGlyph[];
declare const DEFAULT_ICON_GLYPH: TIconGlyph;
declare const ICON_COLORS: TIconColor[];
declare const DEFAULT_ICON_COLOR: TIconColor;
export { ICON_SIZES, DEFAULT_ICON_SIZE, ICON_FONT_SIZES, ICON_GLYPHS, DEFAULT_ICON_GLYPH, ICON_COLORS, DEFAULT_ICON_COLOR, };
