import type { TBaseColorName, TBaseColorShade } from './colors.types';
declare const COLOR_NAMES: TBaseColorName[];
declare const COLOR_SHADES: TBaseColorShade[];
export { COLOR_NAMES, COLOR_SHADES };
