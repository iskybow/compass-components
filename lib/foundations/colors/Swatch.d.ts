import React from 'react';
declare type PSwatch = {
    color: string;
    shade: number;
    colorName?: string;
    variant?: 'noText' | 'bottom' | 'right';
};
declare const Swatch: React.FC<PSwatch>;
export default Swatch;
