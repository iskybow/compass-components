const COLOR_NAMES = [
    'blue',
    'cyan',
    'green',
    'indigo',
    'neutral',
    'orange',
    'purple',
    'red',
    'teal',
    'yellow',
];
const COLOR_SHADES = [100, 200, 300, 400, 500, 600, 700, 800];
export { COLOR_NAMES, COLOR_SHADES };
