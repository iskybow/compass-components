import React from 'react';
import Heading from '../../components/heading';
import Text from '../../components/text';
import Flex from '../../utilities/layout';
import Spacing from '../../utilities/spacing';
import Shape from '../shape';
import { convertToRgb, rgbToHex, rgbToHsl } from '../../shared';
const Swatch = (props) => {
    var _a;
    const { color, shade, colorName, variant = 'right' } = props;
    const rgbString = convertToRgb(color);
    const hexString = rgbToHex(rgbString);
    const hslString = rgbToHsl(rgbString);
    const isRow = variant === 'right';
    const hasText = variant !== 'noText';
    return React.createElement(
        Flex,
        {
            row: isRow,
            alignment: 'stretch',
            padding: isRow ? Spacing.symmetric({ vertical: 50 }) : Spacing.all(50),
            flex: 0,
        },
        React.createElement(
            Flex,
            { alignment: 'flex-end' },
            React.createElement(Shape, {
                className: 'swatch_color',
                radius: 4,
                elevation: 1,
                elevationOnHover: 3,
                width: 140,
                height: 100,
                backgroundColor: rgbString,
            })
        ),
        hasText &&
            React.createElement(
                Flex,
                { flex: 2, padding: Spacing.trbl({ top: 50, right: 0, bottom: 50, left: 75 }) },
                React.createElement(
                    Flex,
                    null,
                    React.createElement(
                        Heading,
                        { element: 'h6', size: 200, margin: isRow ? 'none' : 'bottom' },
                        `${colorName || ''} ${shade}`.trim()
                    )
                ),
                React.createElement(
                    Flex,
                    null,
                    React.createElement(
                        Text,
                        { element: 'p', size: 75, margin: 'none', color: 'secondary' },
                        (_a = hexString.toUpperCase) === null || _a === void 0
                            ? void 0
                            : _a.call(hexString)
                    ),
                    React.createElement(
                        Text,
                        { element: 'p', size: 75, margin: 'none', color: 'secondary' },
                        rgbString
                    ),
                    React.createElement(
                        Text,
                        { element: 'p', size: 75, margin: 'none', color: 'secondary' },
                        hslString
                    )
                )
            )
    );
};
export default Swatch;
